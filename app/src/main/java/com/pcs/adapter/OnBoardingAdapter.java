package com.pcs.adapter;

import android.content.Context;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;
import android.textview.TextViewSFTextRegular;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pcs.R;
import com.pcs.activity.AddOnBoardingActivity;
import com.pcs.model.Transaction;
import com.pcs.util.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OnBoardingAdapter extends RecyclerView.Adapter<OnBoardingAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<Transaction.Data> mData;

    public OnBoardingAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_on_boarding, parent, false);
        return new MyViewHolder(view);
    }

    public void doRefresh(ArrayList<Transaction.Data> arrayList) {
        this.mData = arrayList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Transaction.Data item = mData.get(position);
        holder.tvName.setText(item.name);
        holder.tvAddress.setText(item.farmAddress);
        holder.tvMobileNo.setText(item.contactNo);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextViewSFTextRegular tvName;
        @BindView(R.id.tv_address)
        TextViewSFTextRegular tvAddress;
        @BindView(R.id.tv_mobile_no)
        TextViewSFTextRegular tvMobileNo;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, AddOnBoardingActivity.class);
                    i.putExtra(Constants.ONBOARD_DATA, mData.get(getAdapterPosition()));
                    mContext.startActivity(i);
                }
            });
        }
    }
}