package com.pcs.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class UserModel extends GeneralModel implements Serializable {

    @Expose
    @SerializedName("data")
    public List<Data> data;

    public static class Data {
        @Expose
        @SerializedName("created_at")
        public String createdAt;
        @Expose
        @SerializedName("status")
        public String status;
        @Expose
        @SerializedName("micr")
        public String micr;
        @Expose
        @SerializedName("ifsc")
        public String ifsc;
        @Expose
        @SerializedName("account_no")
        public String accountNo;
        @Expose
        @SerializedName("bank_name")
        public String bankName;
        @Expose
        @SerializedName("comments")
        public String comments;
        @Expose
        @SerializedName("adhar_no")
        public String adharNo;
        @Expose
        @SerializedName("pan_no")
        public String panNo;
        @Expose
        @SerializedName("address")
        public String address;
        @Expose
        @SerializedName("mobile_alter")
        public String mobileAlter;
        @Expose
        @SerializedName("mobile_no")
        public String mobileNo;
        @Expose
        @SerializedName("device_token")
        public String deviceToken;
        @Expose
        @SerializedName("password")
        public String password;
        @Expose
        @SerializedName("email")
        public String email;
        @Expose
        @SerializedName("username")
        public String username;
        @Expose
        @SerializedName("id")
        public String id;
    }
}
