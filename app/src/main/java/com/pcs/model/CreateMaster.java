package com.pcs.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateMaster extends GeneralModel {

    @Expose
    @SerializedName("step")
    public String step;
    @Expose
    @SerializedName("id")
    public int id;
}
