package com.pcs.activity;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pcs.R;
import com.pcs.adapter.TransactionAdapter;
import com.pcs.model.Transaction;
import com.pcs.util.EqualSpacingItemDecoration;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PendingTaskActivity extends BaseActivity {

    @BindView(R.id.rv_pending_task)
    RecyclerView rvPendingTask;

    private TransactionAdapter mAdapter;
    private ArrayList<Transaction.Data> transactionArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_task);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Pending Task");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvPendingTask.setLayoutManager(new LinearLayoutManager(this));
        rvPendingTask.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.VERTICAL));
        getPendingTransaction();
    }

    public void getPendingTransaction() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Transaction> call = getService().getPendingTransaction(0, getUserId());
        call.enqueue(new Callback<Transaction>() {
            @Override
            public void onResponse(Call<Transaction> call, Response<Transaction> response) {
                Transaction transaction = response.body();
                if (checkStatus(transaction)) {
                    transactionArrayList = (ArrayList<Transaction.Data>) transaction.data;
                    setAdapter();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Transaction> call, Throwable t) {
                failureError("get transaction failed");
            }
        });
    }

    private void setAdapter() {
        if (transactionArrayList != null && transactionArrayList.size() > 0) {
            if (mAdapter == null) {
                mAdapter = new TransactionAdapter(this);
            }
            mAdapter.doRefresh(transactionArrayList);
            if (rvPendingTask.getAdapter() == null) {
                rvPendingTask.setAdapter(mAdapter);
            }
        }
    }
}
