package com.pcs.activity;

import android.edittext.EditTextSFTextRegular;
import android.os.Bundle;
import android.textview.TextViewSFTextSemiBold;

import com.pcs.R;
import com.pcs.model.GeneralModel;
import com.pcs.util.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPlanActivity extends BaseActivity {

    @BindView(R.id.tv_date)
    TextViewSFTextSemiBold tvDate;
    @BindView(R.id.et_plan)
    EditTextSFTextRegular etPlan;

    private String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_plan);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Add Plan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent() != null) {
            date = getIntent().getStringExtra(Constants.PLAN_DATE);
        }

        tvDate.setText(date);
    }

    @OnClick(R.id.btn_submit)
    public void onViewClicked() {
        if (isEmpty(getNote())) {
            validationError("Please add plan");
            return;
        }

        addPlan();
    }

    private String getNote() {
        return etPlan.getText().toString().trim();
    }

    public void addPlan() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().addPlan(getUserId(), getNote(), date);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    onBackPressed();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("add plan failed");
            }
        });
    }
}
