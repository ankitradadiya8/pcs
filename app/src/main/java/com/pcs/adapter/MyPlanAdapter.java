package com.pcs.adapter;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.textview.TextViewSFTextRegular;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pcs.R;
import com.pcs.model.Plans;
import com.pcs.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyPlanAdapter extends RecyclerView.Adapter<MyPlanAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<Plans.Data> mData;

    public MyPlanAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_my_plan, parent, false);
        return new MyViewHolder(view);
    }

    public void doRefresh(ArrayList<Plans.Data> plansList) {
        this.mData = plansList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Plans.Data item = mData.get(position);
        holder.tvDate.setText(Utils.changeDateFormat("yyyy-MM-dd hh:mm:ss", "d MMM, yyyy", item.datetime));
        holder.tvNote.setText(item.comment);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_no)
        TextViewSFTextRegular tvNo;
        @BindView(R.id.tv_date)
        TextViewSFTextRegular tvDate;
        @BindView(R.id.tv_note)
        TextViewSFTextRegular tvNote;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}