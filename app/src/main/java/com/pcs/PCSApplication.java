package com.pcs;

import android.annotation.SuppressLint;
import android.provider.Settings;
import androidx.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.util.Log;

import com.pcs.util.Constants;
import com.pcs.util.Preferences;

public class PCSApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        String gaid = Preferences.readString(getApplicationContext(), Constants.GAID, "");
        if (TextUtils.isEmpty(gaid)) {
            @SuppressLint("HardwareIds")
            String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            Preferences.writeString(this, Constants.GAID, android_id);
            Log.e("GAID", android_id);
        }
    }
}
