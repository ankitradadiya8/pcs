package com.pcs.activity;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.textview.TextViewSFTextMedium;
import android.view.View;

import com.pcs.R;
import com.pcs.adapter.OutstandingAdapter;
import com.pcs.model.Outstanding;
import com.pcs.util.EqualSpacingItemDecoration;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OutstandingActivity extends BaseActivity {

    @BindView(R.id.tv_total_collection)
    TextViewSFTextMedium tvTotalCollection;
    @BindView(R.id.tv_no_data)
    TextViewSFTextMedium tvNoData;
    @BindView(R.id.rv_collection)
    RecyclerView rvCollection;

    private OutstandingAdapter mAdapter;
    private ArrayList<Outstanding.Data> collectionArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outstanding);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("View/Outstanding");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvCollection.setLayoutManager(new LinearLayoutManager(this));
        rvCollection.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.VERTICAL));

        getOutstanding();
    }

    public void getOutstanding() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Outstanding> call = getService().getCollection(getUserId());
        call.enqueue(new Callback<Outstanding>() {
            @Override
            public void onResponse(Call<Outstanding> call, Response<Outstanding> response) {
                Outstanding outstanding = response.body();
                if (checkStatus(outstanding)) {
                    collectionArrayList = (ArrayList<Outstanding.Data>) outstanding.data;
                    setAdapter();
                    tvTotalCollection.setText("Total Outstanding Amount : " + outstanding.finalTotal);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Outstanding> call, Throwable t) {
                failureError("get outstanding failed");
            }
        });
    }

    private void setAdapter() {
        if (collectionArrayList != null && collectionArrayList.size() > 0) {
            tvNoData.setVisibility(View.GONE);
            if (mAdapter == null) {
                mAdapter = new OutstandingAdapter(this);
            }
            mAdapter.doRefresh(collectionArrayList);
            if (rvCollection.getAdapter() == null) {
                rvCollection.setAdapter(mAdapter);
            }
        } else {
            tvNoData.setVisibility(View.VISIBLE);
        }
    }
}
