package com.pcs.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Transaction extends GeneralModel implements Serializable {

    @Expose
    @SerializedName("data")
    public List<Data> data;

    public static class Data implements Serializable {
        @Expose
        @SerializedName("collected")
        public String collected;
        @Expose
        @SerializedName("status")
        public String status;
        @Expose
        @SerializedName("step")
        public String step;
        @Expose
        @SerializedName("place_of_suppy")
        public String placeOfSuppy;
        @Expose
        @SerializedName("close_transaction")
        public String closeTransaction;
        @Expose
        @SerializedName("send_request")
        public String sendRequest;
        @Expose
        @SerializedName("document3")
        public String document3;
        @Expose
        @SerializedName("inv_comments")
        public String invComments;
        @Expose
        @SerializedName("inv_authorized_by")
        public String invAuthorizedBy;
        @Expose
        @SerializedName("inv_products")
        public String invProducts;
        @Expose
        @SerializedName("inv_date")
        public String invDate;
        @Expose
        @SerializedName("de_no")
        public String deNo;
        @Expose
        @SerializedName("inv_no")
        public String invNo;
        @Expose
        @SerializedName("document2")
        public String document2;
        @Expose
        @SerializedName("order_comments")
        public String orderComments;
        @Expose
        @SerializedName("authorized_by")
        public String authorizedBy;
        @Expose
        @SerializedName("payment_tearus")
        public String paymentTearus;
        @Expose
        @SerializedName("goods_recieved_by")
        public String goodsRecievedBy;
        @Expose
        @SerializedName("driver_mobile")
        public String driverMobile;
        @Expose
        @SerializedName("driver_name")
        public String driverName;
        @Expose
        @SerializedName("vehicle_no")
        public String vehicleNo;
        @Expose
        @SerializedName("disputed_by")
        public String disputedBy;
        @Expose
        @SerializedName("order_products")
        public String orderProducts;
        @Expose
        @SerializedName("order_date")
        public String orderDate;
        @Expose
        @SerializedName("order_no")
        public String orderNo;
        @Expose
        @SerializedName("apeda_image")
        public String apedaImage;
        @Expose
        @SerializedName("iec_image")
        public String iecImage;
        @Expose
        @SerializedName("gst_image")
        public String gstImage;
        @Expose
        @SerializedName("shop_act_image")
        public String shopActImage;
        @Expose
        @SerializedName("photo_plant_image")
        public String photoPlantImage;
        @Expose
        @SerializedName("residual_free_not")
        public String residualFreeNot;
        @Expose
        @SerializedName("expected_harvesting_date")
        public String expectedHarvestingDate;
        @Expose
        @SerializedName("expected_yield")
        public String expectedYield;
        @Expose
        @SerializedName("watering_date")
        public String wateringDate;
        @Expose
        @SerializedName("eythrel_date")
        public String eythrelDate;
        @Expose
        @SerializedName("land_for_crop")
        public String landForCrop;
        @Expose
        @SerializedName("total_plant")
        public String totalPlant;
        @Expose
        @SerializedName("crop_name")
        public String cropName;
        @Expose
        @SerializedName("no_of_year")
        public String noOfYear;
        @Expose
        @SerializedName("apeda_no")
        public String apedaNo;
        @Expose
        @SerializedName("iec_no")
        public String iecNo;
        @Expose
        @SerializedName("gst_no")
        public String gstNo;
        @Expose
        @SerializedName("shop_act")
        public String shopAct;
        @Expose
        @SerializedName("apmc_image")
        public String apmcImage;
        @Expose
        @SerializedName("apmc_name")
        public String apmcName;
        @Expose
        @SerializedName("passbook_image")
        public String passbookImage;
        @Expose
        @SerializedName("pan_image")
        public String panImage;
        @Expose
        @SerializedName("pan_no")
        public String panNo;
        @Expose
        @SerializedName("id_proof_image")
        public String idProofImage;
        @Expose
        @SerializedName("visiting_card_image")
        public String visitingCardImage;
        @Expose
        @SerializedName("farm_address")
        public String farmAddress;
        @Expose
        @SerializedName("district")
        public String district;
        @Expose
        @SerializedName("taluka")
        public String taluka;
        @Expose
        @SerializedName("village")
        public String village;
        @Expose
        @SerializedName("contact_no")
        public String contactNo;
        @Expose
        @SerializedName("farm_name")
        public String farmName;
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("user_id")
        public String userId;
        @Expose
        @SerializedName("type")
        public String type;
        @Expose
        @SerializedName("id")
        public String id;
    }
}
