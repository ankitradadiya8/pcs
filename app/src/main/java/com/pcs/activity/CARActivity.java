package com.pcs.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.pcs.R;
import com.pcs.util.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CARActivity extends BaseActivity {

    @BindView(R.id.webView)
    WebView webView;

    private String url;
    private String masterId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Crop Analysis Report");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent() != null)
            masterId = getIntent().getStringExtra(Constants.MASTER_ID);

        if (isEmpty(masterId)) {
            finish();
            return;
        }

        url = Constants.BASE_URL + "Home/cra/" + masterId;

        webView.setWebChromeClient(new MyWebChromeClient(this));
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                showProgress();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                hideProgress();
            }
        });
        webView.clearCache(true);
        webView.clearHistory();
        webView.getSettings().setJavaScriptEnabled(true);

        webView.loadUrl(url);
    }

    private class MyWebChromeClient extends WebChromeClient {
        Context context;

        public MyWebChromeClient(Context context) {
            super();
            this.context = context;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
