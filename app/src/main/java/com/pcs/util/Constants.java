package com.pcs.util;

public class Constants {

    //public static final String BASE_URL = "http://u219644575-primecustomer-com.hostingerapp.com/";
    public static final String BASE_URL = "http://primecustomer.site/index.php/";
    public static final String LOGIN = "login";
    public static final String SIGNUP = "register";
    public static final String ADD_NOTE = "Note";
    public static final String CREATE_MASTER = "Master/create_master";
    public static final String TRANSACTION = "Transaction/index";
    public static final String NEW_TRANSACTION= "Home/copy_record";
    public static final String ADD_HELP = "Help/add_help";
    public static final String GET_PLANS = "Note/get_note";
    public static final String ADD_ON_BOARD = "Master/on_board";
    public static final String GET_ON_BOARD = "Master/board_data";
    public static final String VIEW_COLLECTION = "Transaction/view_collection";
    public static final String SEARCH_TRANSACTION = "Transaction/complete_list";

    public static final int DEVICE_TYPE = 2; // for android

    public static final String SFTEXT_REGULAR = "font/roboto_regular.ttf";
    public static final String SFTEXT_MEDIUM = "font/roboto_medium.ttf";
    public static final String SFTEXT_SEMIBOLD = "font/roboto_bold.ttf";

    public static final String FCM_TOKEN = "fcm_token";
    public static final String FROM_LOGIN = "from login";
    public static final String LOGIN_FINISH = "login_finish";
    public static final String USER_DATA = "user_data";
    public static final String PROFILE_DATA = "profile_data";
    public static final String IS_LOGIN = "isLogin";
    public static final String POLICY_URL = "policy_url";
    public static final String SCREEN_NAME = "screen_name";
    public static final String IS_EDIT = "isEdit";
    public static final String PLAN_DATE = "Plan Date";
    public static final String GAID = "gaid";

    public static final String FARMER = "farmers";
    public static final String VENDOR = "vendors";
    public static final String MASTER_ID = "masterId";
    public static final String ONBOARD_DATA = "onboardData";
}
