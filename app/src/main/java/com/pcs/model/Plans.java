package com.pcs.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Plans extends GeneralModel {

    @Expose
    @SerializedName("data")
    public List<Data> data;

    public static class Data {
        @Expose
        @SerializedName("datetime")
        public String datetime;
        @Expose
        @SerializedName("comment")
        public String comment;
        @Expose
        @SerializedName("user_id")
        public String userId;
        @Expose
        @SerializedName("id")
        public String id;
    }
}
