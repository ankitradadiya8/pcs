package com.pcs.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.button.ButtonSFTextRegular;
import android.button.ButtonSFTextSemiBold;
import android.content.Intent;
import android.edittext.EditTextSFTextRegular;
import android.net.Uri;
import android.os.Bundle;
import android.textview.AutoCompleteSFTextView;
import android.textview.TextViewSFTextRegular;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.pcs.R;
import com.pcs.model.CreateMaster;
import com.pcs.model.Transaction;
import com.pcs.util.Constants;
import com.pcs.util.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddOnBoardingActivity extends BaseActivity {

    @BindView(R.id.btn_farmer_form_submit)
    ButtonSFTextSemiBold btnFarmerFormSubmit;
    @BindView(R.id.et_name)
    EditTextSFTextRegular etName;
    @BindView(R.id.et_farm_name)
    EditTextSFTextRegular etFarmName;
    @BindView(R.id.et_mo_no)
    EditTextSFTextRegular etMoNo;
    @BindView(R.id.et_village)
    EditTextSFTextRegular etVillage;
    @BindView(R.id.et_taluka)
    EditTextSFTextRegular etTaluka;
    @BindView(R.id.et_district)
    EditTextSFTextRegular etDistrict;
    @BindView(R.id.et_address)
    EditTextSFTextRegular etAddress;
    @BindView(R.id.tv_visiting_card)
    EditTextSFTextRegular tvVisitingCard;
    @BindView(R.id.btn_visiting_card)
    ButtonSFTextRegular btnVisitingCard;
    @BindView(R.id.tv_id_card)
    EditTextSFTextRegular tvIdCard;
    @BindView(R.id.btn_id_card)
    ButtonSFTextRegular btnIdCard;
    @BindView(R.id.tv_pan_card)
    EditTextSFTextRegular tvPanCard;
    @BindView(R.id.btn_pan_card)
    ButtonSFTextRegular btnPanCard;
    @BindView(R.id.tv_bank_details)
    EditTextSFTextRegular tvBankDetails;
    @BindView(R.id.btn_bank_details)
    ButtonSFTextRegular btnBankDetails;
    @BindView(R.id.tv_apmc)
    EditTextSFTextRegular tvApmc;
    @BindView(R.id.btn_apmc)
    ButtonSFTextRegular btnApmc;
    @BindView(R.id.tv_shop_act)
    EditTextSFTextRegular tvShopAct;
    @BindView(R.id.btn_shop_act)
    ButtonSFTextRegular btnShopAct;
    @BindView(R.id.tv_gst)
    EditTextSFTextRegular tvGst;
    @BindView(R.id.btn_gst)
    ButtonSFTextRegular btnGst;
    @BindView(R.id.tv_iec)
    EditTextSFTextRegular tvIec;
    @BindView(R.id.btn_iec)
    ButtonSFTextRegular btnIec;
    @BindView(R.id.tv_apeda)
    EditTextSFTextRegular tvApeda;
    @BindView(R.id.btn_apeda)
    ButtonSFTextRegular btnApeda;
    @BindView(R.id.et_no_years)
    EditTextSFTextRegular etNoYears;
    @BindView(R.id.et_crop_name)
    AutoCompleteSFTextView etCropName;
    @BindView(R.id.img_add)
    ImageView imgAdd;
    @BindView(R.id.tv_crop_list)
    TextViewSFTextRegular tvCropList;
    @BindView(R.id.et_no_plants)
    EditTextSFTextRegular etNoPlants;
    @BindView(R.id.et_land_used)
    EditTextSFTextRegular etLandUsed;
    @BindView(R.id.et_eythrel_date)
    EditTextSFTextRegular etEythrelDate;
    @BindView(R.id.et_watering_date)
    EditTextSFTextRegular etWateringDate;
    @BindView(R.id.et_expected_yield)
    EditTextSFTextRegular etExpectedYield;
    @BindView(R.id.et_harvesting_date)
    EditTextSFTextRegular etHarvestingDate;
    @BindView(R.id.et_residual)
    AutoCompleteSFTextView etResidual;
    @BindView(R.id.tv_photos_plants)
    EditTextSFTextRegular tvPhotosPlants;
    @BindView(R.id.btn_photos_plants)
    ButtonSFTextRegular btnPhotosPlants;
    @BindView(R.id.ll_create_master)
    LinearLayout llCreateMaster;

    private String boardId = "";
    private Transaction.Data onBoardData;
    private String cropName = "";
    String[] fruits = {"Apple", "Banana", "Cherry", "Date", "Grape", "Kiwi", "Mango", "Pear", "Pomegranate"};
    String[] options = {"Yes", "No"};
    private File docVCard, docICard, docPanCard, docBank, docApmc, docShopAct, docGst;
    private File docIec, docApeda, docPhotos;
    private String reqCodeImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_on_boarding);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Create OnBoard");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent() != null && getIntent().getExtras() != null) {
            onBoardData = (Transaction.Data) getIntent().getSerializableExtra(Constants.ONBOARD_DATA);
            boardId = onBoardData.id;
            setData(onBoardData);
        }
        llCreateMaster.setVisibility(View.VISIBLE);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, fruits);
        etCropName.setThreshold(1);
        etCropName.setAdapter(adapter);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, options);
        etResidual.setThreshold(1);
        etResidual.setAdapter(adapter1);
    }

    private void setData(Transaction.Data transaction) {
        etName.setText(transaction.name);
        etFarmName.setText(transaction.farmName);
        etMoNo.setText(transaction.contactNo);
        etVillage.setText(transaction.village);
        etTaluka.setText(transaction.taluka);
        etDistrict.setText(transaction.district);
        etAddress.setText(transaction.farmAddress);
        cropName = transaction.cropName;
        tvCropList.setText(cropName);
        tvVisitingCard.setHint(transaction.visitingCardImage);
        tvIdCard.setHint(transaction.idProofImage);
        tvPanCard.setText(transaction.panNo);
        tvPanCard.setHint(transaction.panImage);
        tvBankDetails.setHint(transaction.passbookImage);
        tvApmc.setText(transaction.apmcName);
        tvApmc.setHint(transaction.apmcImage);
        tvShopAct.setHint(transaction.shopActImage);
        tvShopAct.setText(transaction.shopAct);
        tvGst.setHint(transaction.gstImage);
        tvGst.setText(transaction.gstNo);
        tvIec.setHint(transaction.iecImage);
        tvIec.setText(transaction.iecNo);
        tvApeda.setHint(transaction.apedaImage);
        tvApeda.setText(transaction.apedaNo);
        etNoYears.setText(transaction.noOfYear);
        etNoPlants.setText(transaction.totalPlant);
        etLandUsed.setText(transaction.landForCrop);
        if (!transaction.eythrelDate.contains("0000-00-00") && !transaction.eythrelDate.contains("1970-01-01"))
            etEythrelDate.setText(decodeDate(transaction.eythrelDate));
        if (!transaction.wateringDate.contains("0000-00-00") && !transaction.wateringDate.contains("1970-01-01"))
            etWateringDate.setText(decodeDate(transaction.wateringDate));
        if (!transaction.expectedHarvestingDate.contains("0000-00-00") && !transaction.expectedHarvestingDate.contains("1970-01-01"))
            etHarvestingDate.setText(decodeDate(transaction.expectedHarvestingDate));
        etExpectedYield.setText(transaction.expectedYield);
        etResidual.setText(transaction.residualFreeNot);
        tvPhotosPlants.setHint(transaction.photoPlantImage);
    }

    @OnClick({R.id.btn_farmer_form_submit, R.id.et_crop_name, R.id.img_add, R.id.btn_visiting_card, R.id.btn_id_card, R.id.btn_pan_card,
            R.id.btn_bank_details, R.id.btn_apmc, R.id.btn_shop_act, R.id.btn_gst, R.id.btn_iec, R.id.btn_apeda, R.id.btn_photos_plants,
            R.id.et_residual, R.id.et_eythrel_date, R.id.et_watering_date, R.id.et_harvesting_date})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_farmer_form_submit:
                addOnBoard();
                break;
            case R.id.et_residual:
                etResidual.showDropDown();
                break;
            case R.id.btn_visiting_card:
                reqCodeImage = "btn_visiting_card";
                checkPermission();
                break;
            case R.id.btn_id_card:
                reqCodeImage = "btn_id_card";
                checkPermission();
                break;
            case R.id.btn_pan_card:
                reqCodeImage = "btn_pan_card";
                checkPermission();
                break;
            case R.id.btn_bank_details:
                reqCodeImage = "btn_bank_details";
                checkPermission();
                break;
            case R.id.btn_apmc:
                reqCodeImage = "btn_apmc";
                checkPermission();
                break;
            case R.id.btn_shop_act:
                reqCodeImage = "btn_shop_act";
                checkPermission();
                break;
            case R.id.btn_gst:
                reqCodeImage = "btn_gst";
                checkPermission();
                break;
            case R.id.btn_iec:
                reqCodeImage = "btn_iec";
                checkPermission();
                break;
            case R.id.btn_apeda:
                reqCodeImage = "btn_apeda";
                checkPermission();
                break;
            case R.id.btn_photos_plants:
                reqCodeImage = "btn_photos_plants";
                checkPermission();
                break;
            case R.id.et_crop_name:
                etCropName.showDropDown();
                break;
            case R.id.et_eythrel_date:
                showDatePicker(etEythrelDate);
                break;
            case R.id.et_watering_date:
                showDatePicker(etWateringDate);
                break;
            case R.id.et_harvesting_date:
                showDatePicker(etHarvestingDate);
                break;
            case R.id.img_add:
                if (!isEmpty(getCropName())) {
                    if (isEmpty(cropName)) {
                        cropName = getCropName();
                    } else {
                        cropName = cropName + ", " + getCropName();
                    }
                }

                if (!isEmpty(cropName)) {
                    etCropName.setText("");
                    tvCropList.setText(cropName);
                    tvCropList.setVisibility(View.VISIBLE);
                } else {
                    tvCropList.setVisibility(View.GONE);
                }
                break;
        }
    }

    public void addOnBoard() {
        if (!isNetworkConnected())
            return;

        showProgress();
        MultipartBody.Part bodyVCard = getBodyPart(docVCard, "visiting_card_image");
        MultipartBody.Part bodyICard = getBodyPart(docICard, "id_proof_image");
        MultipartBody.Part bodyPanCard = getBodyPart(docPanCard, "pan_image");
        MultipartBody.Part bodyBanks = getBodyPart(docBank, "passbook_image");
        MultipartBody.Part bodyApmc = getBodyPart(docApmc, "apmc_image");
        MultipartBody.Part bodyShopAct = getBodyPart(docShopAct, "shop_act_image");
        MultipartBody.Part bodyGst = getBodyPart(docGst, "gst_image");
        MultipartBody.Part bodyIec = getBodyPart(docIec, "iec_image");
        MultipartBody.Part bodyApeda = getBodyPart(docApeda, "apeda_image");
        MultipartBody.Part bodyPhotoplant = getBodyPart(docPhotos, "photo_plant_image");

        RequestBody name = RequestBody.create(MultipartBody.FORM, getName());
        RequestBody farmName = RequestBody.create(MultipartBody.FORM, getFarmName());
        RequestBody address = RequestBody.create(MultipartBody.FORM, getAddress());
        RequestBody mobileNo = RequestBody.create(MultipartBody.FORM, getMobileNo());
        RequestBody village = RequestBody.create(MultipartBody.FORM, getVillage());
        RequestBody taluka = RequestBody.create(MultipartBody.FORM, getTaluka());
        RequestBody district = RequestBody.create(MultipartBody.FORM, getDistrict());
        RequestBody pancardNo = RequestBody.create(MultipartBody.FORM, getPanCardNo());
        RequestBody apmcName = RequestBody.create(MultipartBody.FORM, getAPMC());
        RequestBody shopAct = RequestBody.create(MultipartBody.FORM, getShopAct());
        RequestBody gstNo = RequestBody.create(MultipartBody.FORM, getGST());
        RequestBody iecNo = RequestBody.create(MultipartBody.FORM, getIEC());
        RequestBody apedaNo = RequestBody.create(MultipartBody.FORM, getApeda());
        RequestBody noOfYear = RequestBody.create(MultipartBody.FORM, getNoOfYrs());
        RequestBody cropName = RequestBody.create(MultipartBody.FORM, getCropName());
        RequestBody totalPlant = RequestBody.create(MultipartBody.FORM, getTotalPlant());
        RequestBody landForCrop = RequestBody.create(MultipartBody.FORM, getLandForCrop());
        RequestBody eythrelDate = RequestBody.create(MultipartBody.FORM, getEthyralDate());
        RequestBody wateringDate = RequestBody.create(MultipartBody.FORM, getWateringDate());
        RequestBody expectedYield = RequestBody.create(MultipartBody.FORM, getExpectedYield());
        RequestBody harvestingDate = RequestBody.create(MultipartBody.FORM, getHarvestingDate());
        RequestBody residualFreeOrNot = RequestBody.create(MultipartBody.FORM, getResFreeOrNot());

        RequestBody userId = RequestBody.create(MultipartBody.FORM, getUserId());
        RequestBody boardID = RequestBody.create(MultipartBody.FORM, boardId);

        Call<CreateMaster> call = getService().addOnBoard(name, mobileNo, farmName, village, taluka, district, address,
                pancardNo, apmcName, shopAct, gstNo, iecNo, apedaNo, noOfYear, cropName, totalPlant, landForCrop, eythrelDate, wateringDate,
                expectedYield, harvestingDate, residualFreeOrNot, bodyVCard, bodyICard, bodyPanCard, bodyBanks, bodyApmc, bodyShopAct,
                bodyGst, bodyIec, bodyApeda, bodyPhotoplant, userId, boardID);
        call.enqueue(new Callback<CreateMaster>() {
            @Override
            public void onResponse(Call<CreateMaster> call, Response<CreateMaster> response) {
                CreateMaster createMaster = response.body();
                if (checkStatus(createMaster)) {
                    boardId = String.valueOf(createMaster.id);
                    toastMessage("OnBoard Added successfully");
                    finish();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<CreateMaster> call, Throwable t) {
                failureError("update onboard failed");
            }
        });
    }

    private MultipartBody.Part getBodyPart(File docPhotos, String imageName) {
        if (docPhotos != null && imageName != null) {
            Uri selectedUri = Uri.fromFile(docPhotos);
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());

            RequestBody requestFile = null;
            if (mimeType != null) {
                requestFile = RequestBody.create(MediaType.parse(mimeType), docPhotos);
            }

            if (requestFile != null) {
                return MultipartBody.Part.createFormData(imageName, docPhotos.getName(), requestFile);
            }
        }
        return null;
    }

    private void showDatePicker(final EditText editText) {
        Calendar myCalendar = Calendar.getInstance();

        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                cal.set(Calendar.MONTH, month);
                String date = new SimpleDateFormat("d MMM, yyyy").format(cal.getTime());
                editText.setText(date);
            }
        }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private String getName() {
        return etName.getText().toString().trim();
    }

    private String getFarmName() {
        return etFarmName.getText().toString().trim();
    }

    private String getVillage() {
        return etVillage.getText().toString().trim();
    }

    private String getDistrict() {
        return etDistrict.getText().toString().trim();
    }

    private String getTaluka() {
        return etTaluka.getText().toString().trim();
    }

    private String getAddress() {
        return etAddress.getText().toString().trim();
    }

    private String getMobileNo() {
        return etMoNo.getText().toString().trim();
    }

    private String getCropName() {
        return etCropName.getText().toString().trim();
    }

    private String getPanCardNo() {
        return tvPanCard.getText().toString().trim();
    }

    private String getAPMC() {
        return tvApmc.getText().toString().trim();
    }

    private String getShopAct() {
        return tvShopAct.getText().toString().trim();
    }

    private String getGST() {
        return tvGst.getText().toString().trim();
    }

    private String getIEC() {
        return tvIec.getText().toString().trim();
    }

    private String getApeda() {
        return tvApeda.getText().toString().trim();
    }

    private String getNoOfYrs() {
        return etNoYears.getText().toString().trim();
    }

    private String getTotalPlant() {
        return etNoPlants.getText().toString().trim();
    }

    private String getLandForCrop() {
        return etLandUsed.getText().toString().trim();
    }

    private String getEthyralDate() {
        return etEythrelDate.getText().toString().trim();
    }

    private String getWateringDate() {
        return etWateringDate.getText().toString().trim();
    }

    private String getExpectedYield() {
        return etExpectedYield.getText().toString().trim();
    }

    private String getHarvestingDate() {
        return etHarvestingDate.getText().toString().trim();
    }

    private String getResFreeOrNot() {
        return etResidual.getText().toString().trim();
    }

    private void checkPermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            CropImage.activity()
                                    .setGuidelines(CropImageView.Guidelines.ON)
                                    .start(AddOnBoardingActivity.this);
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            toastMessage("Please give permission");
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                switch (reqCodeImage) {
                    case "btn_visiting_card":
                        docVCard = new File(resultUri.getPath());
                        tvVisitingCard.setHint(docVCard.getName());
                        break;
                    case "btn_id_card":
                        docICard = new File(resultUri.getPath());
                        tvIdCard.setHint(docICard.getName());
                        break;
                    case "btn_pan_card":
                        docPanCard = new File(resultUri.getPath());
                        tvPanCard.setHint(docPanCard.getName());
                        break;
                    case "btn_bank_details":
                        docBank = new File(resultUri.getPath());
                        tvBankDetails.setHint(docBank.getName());
                        break;
                    case "btn_apmc":
                        docApmc = new File(resultUri.getPath());
                        tvApmc.setText(docApmc.getName());
                        break;
                    case "btn_shop_act":
                        docShopAct = new File(resultUri.getPath());
                        tvShopAct.setHint(docShopAct.getName());
                        break;
                    case "btn_gst":
                        docGst = new File(resultUri.getPath());
                        tvGst.setHint(docGst.getName());
                        break;
                    case "btn_iec":
                        docIec = new File(resultUri.getPath());
                        tvIec.setHint(docIec.getName());
                        break;
                    case "btn_apeda":
                        docApeda = new File(resultUri.getPath());
                        tvApeda.setHint(docApeda.getName());
                        break;
                    case "btn_photos_plants":
                        docPhotos = new File(resultUri.getPath());
                        tvPhotosPlants.setHint(docPhotos.getName());
                        break;
                }
            }
        }
    }

    private String encodeDate(String date) {
        return Utils.changeDateFormat("d MMM, yyyy", "yyyy-MM-dd hh:mm:ss", date);
    }

    private String decodeDate(String date) {
        return Utils.changeDateFormat("yyyy-MM-dd hh:mm:ss", "d MMM, yyyy", date);
    }
}
