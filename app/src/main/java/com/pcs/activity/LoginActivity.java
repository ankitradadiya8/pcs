package com.pcs.activity;

import android.app.Dialog;
import android.edittext.EditTextSFTextRegular;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pcs.R;
import com.pcs.model.UserModel;
import com.pcs.util.Constants;
import com.pcs.util.Preferences;
import com.pcs.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.et_email)
    EditTextSFTextRegular etEmail;
    @BindView(R.id.et_password)
    EditTextSFTextRegular etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        if (isLogin()) {
            gotoMainActivity();
            return;
        }
    }

    public void login() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Object> call = getService().login(getEmail(), getPassword(), getDeviceToken());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String json = new Gson().toJson(response.body());
                if (checkStatus(json)) {
                    UserModel userModel = new Gson().fromJson(json, UserModel.class);
                    Preferences.saveUserData(LoginActivity.this, userModel.data.get(0));
                    Preferences.writeBoolean(LoginActivity.this, Constants.IS_LOGIN, true);
                    gotoMainActivity();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("login failed");
            }
        });
    }

    private String getEmail() {
        return etEmail.getText().toString().trim();
    }

    private String getPassword() {
        return etPassword.getText().toString().trim();
    }

    public boolean validLoginData() {
        if (!isValidEmail(getEmail())) {
            validationError("Enter Valid Email");
            return false;
        }

        if (isEmpty(getPassword())) {
            validationError("Enter Password");
            return false;
        }

        return true;
    }

    @OnClick({R.id.btnLogin, R.id.btnRegister, R.id.tv_forgot_pass, R.id.tv_need_help})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                if (validLoginData()) {
                    login();
                }
                //redirectActivity(MainActivity.class);
                break;
            case R.id.btnRegister:
                redirectActivity(RegisterActivity.class);
                break;
            case R.id.tv_forgot_pass:
                showForgotPasswordDialog();
                break;
            case R.id.tv_need_help:
                showNeedHelpDialog();
                break;
        }
    }

    public void showForgotPasswordDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_forgot_password);
        dialog.setCancelable(true);
        Button btnCancel = dialog.findViewById(R.id.btn_cancel);
        Button btnReset = dialog.findViewById(R.id.btn_reset);
        final EditText etEmail = dialog.findViewById(R.id.et_email);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidEmail(etEmail.getText().toString())) {
                    Utils.hideSoftKeyboard(LoginActivity.this);
                    //forgotPassword(etEmail.getText().toString(), false);
                    dialog.dismiss();
                } else {
                    Toast.makeText(LoginActivity.this, "Enter valid email", Toast.LENGTH_SHORT).show();
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }
}
