package com.pcs.adapter;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.textview.TextViewSFTextRegular;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pcs.R;
import com.pcs.model.Outstanding;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OutstandingAdapter extends RecyclerView.Adapter<OutstandingAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<Outstanding.Data> mData;

    public OutstandingAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_outstading, parent, false);
        return new MyViewHolder(view);
    }

    public void doRefresh(ArrayList<Outstanding.Data> arrayList) {
        this.mData = arrayList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Outstanding.Data item = mData.get(position);
        holder.tvName.setText(item.username);
        holder.tvType.setText(item.type);
        holder.tvOutstanding.setText("Outstanding Amount : " + item.total);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextViewSFTextRegular tvName;
        @BindView(R.id.tv_type)
        TextViewSFTextRegular tvType;
        @BindView(R.id.tv_outstanding)
        TextViewSFTextRegular tvOutstanding;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}