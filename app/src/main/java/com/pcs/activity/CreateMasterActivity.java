package com.pcs.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.button.ButtonSFTextRegular;
import android.button.ButtonSFTextSemiBold;
import android.content.DialogInterface;
import android.content.Intent;
import android.edittext.EditTextSFTextRegular;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.textview.AutoCompleteSFTextView;
import android.textview.TextViewSFTextMedium;
import android.textview.TextViewSFTextRegular;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.pcs.R;
import com.pcs.adapter.ProductsAdapter;
import com.pcs.model.CreateMaster;
import com.pcs.model.GeneralModel;
import com.pcs.model.Product;
import com.pcs.model.Transaction;
import com.pcs.util.Constants;
import com.pcs.util.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateMasterActivity extends BaseActivity {

    @BindView(R.id.radiogroup)
    RadioGroup radiogroup;
    @BindView(R.id.btn_p_document2)
    ButtonSFTextRegular btnFarmerUploadDoc;
    @BindView(R.id.btn_farmer_form_submit)
    ButtonSFTextSemiBold btnFarmerFormSubmit;
    @BindView(R.id.et_order_no)
    EditTextSFTextRegular etOrderNo;
    @BindView(R.id.et_grn_date)
    EditTextSFTextRegular etGrnDate;
    @BindView(R.id.rv_grn_products)
    RecyclerView rvGrnProducts;
    @BindView(R.id.et_vehicle_no)
    EditTextSFTextRegular etVehicleNo;
    @BindView(R.id.et_driver_name)
    EditTextSFTextRegular etDriverName;
    @BindView(R.id.et_driver_mo_no)
    EditTextSFTextRegular etDriverMoNo;
    @BindView(R.id.btn_grn_submit)
    ButtonSFTextSemiBold btnGrnSubmit;
    @BindView(R.id.ll_GRN)
    LinearLayout llGRN;
    @BindView(R.id.btn_send_payment_req)
    TextView btnSendPaymentReq;
    @BindView(R.id.ll_farmer)
    LinearLayout llFarmer;
    @BindView(R.id.btn_s_document2)
    ButtonSFTextRegular btnVendorUploadForm;
    @BindView(R.id.et_delivery_no)
    EditTextSFTextRegular etDeliveryNo;
    @BindView(R.id.et_challan_date)
    EditTextSFTextRegular etChallanDate;
    @BindView(R.id.rv_challan_products)
    RecyclerView rvChallanProducts;
    @BindView(R.id.ll_challan)
    LinearLayout llChallan;
    @BindView(R.id.ll_vendor)
    LinearLayout llVendor;
    @BindView(R.id.ll_main)
    LinearLayout llMain;
    @BindView(R.id.btn_grn_product)
    ButtonSFTextRegular btnGrnProduct;
    @BindView(R.id.tv_grn_total)
    TextView tvGRNTotal;
    @BindView(R.id.ll_grn_product)
    LinearLayout llGrnProduct;
    @BindView(R.id.ll_challan_product)
    LinearLayout llChallanProduct;
    @BindView(R.id.tv_challan_total)
    TextViewSFTextMedium tvChallanTotal;
    @BindView(R.id.btn_challan_product)
    ButtonSFTextRegular btnChallanProduct;
    @BindView(R.id.radio_farmer)
    RadioButton radioFarmer;
    @BindView(R.id.radio_vendor)
    RadioButton radioVendor;
    @BindView(R.id.tv_p_docs2)
    TextViewSFTextRegular tvPDocs2;
    @BindView(R.id.tv_grn_form)
    TextViewSFTextMedium tvGrnForm;
    @BindView(R.id.tv_challan_form)
    TextViewSFTextMedium tvChallanForm;
    @BindView(R.id.tv_s_docs2)
    TextViewSFTextRegular tvSDocs2;
    @BindView(R.id.btn_challan_submit)
    ButtonSFTextSemiBold btnChallanSubmit;
    @BindView(R.id.tv_purchase_invoice)
    TextViewSFTextMedium tvPurchaseInvoice;
    @BindView(R.id.ll_purchase_invoice)
    LinearLayout llPurchaseInvoice;
    @BindView(R.id.btn_purchase_invoice_submit)
    ButtonSFTextSemiBold btnPurchaseInvoiceSubmit;
    @BindView(R.id.btn_close_transaction)
    TextViewSFTextMedium btnCloseTransaction;
    @BindView(R.id.tv_sales_invoice)
    TextViewSFTextMedium tvSalesInvoice;
    @BindView(R.id.btn_sales_invoice_submit)
    ButtonSFTextSemiBold btnSalesInvoiceSubmit;
    @BindView(R.id.ll_sales_invoice)
    LinearLayout llSalesInvoice;
    @BindView(R.id.img_add)
    ImageView imgAdd;
    @BindView(R.id.et_farmer_comments2)
    EditTextSFTextRegular etFarmerComments2;
    @BindView(R.id.et_farmer_comments3)
    EditTextSFTextRegular etFarmerComments3;
    @BindView(R.id.et_vendor_comments2)
    EditTextSFTextRegular etVendorComments2;
    @BindView(R.id.et_goods_receivedby)
    EditTextSFTextRegular etGoodsReceivedby;
    @BindView(R.id.et_farmer_payment_terms)
    EditTextSFTextRegular etFarmerPaymentTerms;
    @BindView(R.id.et_grn_authorised)
    EditTextSFTextRegular etGrnAuthorised;
    @BindView(R.id.et_p_invoice_no)
    EditTextSFTextRegular etPInvoiceNo;
    @BindView(R.id.et_p_date)
    EditTextSFTextRegular etPDate;
    @BindView(R.id.ll_p_invoice_product)
    LinearLayout llPInvoiceProduct;
    @BindView(R.id.rv_p_invoice_products)
    RecyclerView rvPInvoiceProducts;
    @BindView(R.id.tv_p_invoice_total)
    TextViewSFTextMedium tvPInvoiceTotal;
    @BindView(R.id.btn_p_invoice_product)
    ButtonSFTextRegular btnPInvoiceProduct;
    @BindView(R.id.et_pinvoice_authorised)
    EditTextSFTextRegular etPinvoiceAuthorised;
    @BindView(R.id.et_dispatched_by)
    EditTextSFTextRegular etDispatchedBy;
    @BindView(R.id.et_s_vehicle_no)
    EditTextSFTextRegular etSVehicleNo;
    @BindView(R.id.et_s_driver_name)
    EditTextSFTextRegular etSDriverName;
    @BindView(R.id.et_s_driver_mo_no)
    EditTextSFTextRegular etSDriverMoNo;
    @BindView(R.id.et_challan_authorised)
    EditTextSFTextRegular etChallanAuthorised;
    @BindView(R.id.et_s_invoice_no)
    EditTextSFTextRegular etSInvoiceNo;
    @BindView(R.id.et_s_date)
    EditTextSFTextRegular etSDate;
    @BindView(R.id.et_place)
    EditTextSFTextRegular etPlace;
    @BindView(R.id.et_s_delivery_no)
    EditTextSFTextRegular etSDeliveryNo;
    @BindView(R.id.ll_s_invoice_product)
    LinearLayout llSInvoiceProduct;
    @BindView(R.id.rv_s_invoice_products)
    RecyclerView rvSInvoiceProducts;
    @BindView(R.id.tv_s_invoice_total)
    TextViewSFTextMedium tvSInvoiceTotal;
    @BindView(R.id.btn_s_invoice_product)
    ButtonSFTextRegular btnSInvoiceProduct;
    @BindView(R.id.et_sinvoice_authorised)
    EditTextSFTextRegular etSinvoiceAuthorised;
    @BindView(R.id.et_vendor_comments3)
    EditTextSFTextRegular etVendorComments3;
    @BindView(R.id.tv_p_docs3)
    TextViewSFTextRegular tvPDocs3;
    @BindView(R.id.btn_p_document3)
    ButtonSFTextRegular btnPDocument3;
    @BindView(R.id.tv_s_docs3)
    TextViewSFTextRegular tvSDocs3;
    @BindView(R.id.btn_s_document3)
    ButtonSFTextRegular btnSDocument3;
    @BindView(R.id.btn_new_transaction)
    ButtonSFTextSemiBold btnNewTransaction;
    @BindView(R.id.et_name)
    EditTextSFTextRegular etName;
    @BindView(R.id.et_farm_name)
    EditTextSFTextRegular etFarmName;
    @BindView(R.id.et_mo_no)
    EditTextSFTextRegular etMoNo;
    @BindView(R.id.et_village)
    EditTextSFTextRegular etVillage;
    @BindView(R.id.et_taluka)
    EditTextSFTextRegular etTaluka;
    @BindView(R.id.et_district)
    EditTextSFTextRegular etDistrict;
    @BindView(R.id.et_address)
    EditTextSFTextRegular etAddress;
    @BindView(R.id.tv_visiting_card)
    EditTextSFTextRegular tvVisitingCard;
    @BindView(R.id.btn_visiting_card)
    ButtonSFTextRegular btnVisitingCard;
    @BindView(R.id.tv_id_card)
    EditTextSFTextRegular tvIdCard;
    @BindView(R.id.btn_id_card)
    ButtonSFTextRegular btnIdCard;
    @BindView(R.id.tv_pan_card)
    EditTextSFTextRegular tvPanCard;
    @BindView(R.id.btn_pan_card)
    ButtonSFTextRegular btnPanCard;
    @BindView(R.id.tv_bank_details)
    EditTextSFTextRegular tvBankDetails;
    @BindView(R.id.btn_bank_details)
    ButtonSFTextRegular btnBankDetails;
    @BindView(R.id.tv_apmc)
    EditTextSFTextRegular tvApmc;
    @BindView(R.id.btn_apmc)
    ButtonSFTextRegular btnApmc;
    @BindView(R.id.tv_shop_act)
    EditTextSFTextRegular tvShopAct;
    @BindView(R.id.btn_shop_act)
    ButtonSFTextRegular btnShopAct;
    @BindView(R.id.tv_gst)
    EditTextSFTextRegular tvGst;
    @BindView(R.id.btn_gst)
    ButtonSFTextRegular btnGst;
    @BindView(R.id.tv_iec)
    EditTextSFTextRegular tvIec;
    @BindView(R.id.btn_iec)
    ButtonSFTextRegular btnIec;
    @BindView(R.id.tv_apeda)
    EditTextSFTextRegular tvApeda;
    @BindView(R.id.btn_apeda)
    ButtonSFTextRegular btnApeda;
    @BindView(R.id.et_no_years)
    EditTextSFTextRegular etNoYears;
    @BindView(R.id.et_crop_name)
    AutoCompleteSFTextView etCropName;
    @BindView(R.id.tv_crop_list)
    TextViewSFTextRegular tvCropList;
    @BindView(R.id.et_no_plants)
    EditTextSFTextRegular etNoPlants;
    @BindView(R.id.et_land_used)
    EditTextSFTextRegular etLandUsed;
    @BindView(R.id.et_eythrel_date)
    EditTextSFTextRegular etEythrelDate;
    @BindView(R.id.et_watering_date)
    EditTextSFTextRegular etWateringDate;
    @BindView(R.id.et_expected_yield)
    EditTextSFTextRegular etExpectedYield;
    @BindView(R.id.et_harvesting_date)
    EditTextSFTextRegular etHarvestingDate;
    @BindView(R.id.et_residual)
    AutoCompleteSFTextView etResidual;
    @BindView(R.id.tv_photos_plants)
    EditTextSFTextRegular tvPhotosPlants;
    @BindView(R.id.btn_photos_plants)
    ButtonSFTextRegular btnPhotosPlants;
    @BindView(R.id.ll_create_master)
    LinearLayout llCreateMaster;

    private RadioButton radioButton;
    private boolean isFarmer;
    private ArrayList<Product> orderProductList;
    private ArrayList<Product> invoiceProductList;
    private ProductsAdapter orderAdapter;
    private ProductsAdapter invoiceAdapter;
    private String type = "";
    private File documentFile1, documentFile2, documentFile3, docVCard, docICard, docPanCard, docBank, docApmc, docShopAct, docGst;
    private File docIec, docApeda, docPhotos;
    private String masterId = "";
    private String ordersProducts = "";
    private String invProducts = "";
    String[] fruits = {"Apple", "Banana", "Cherry", "Date", "Grape", "Kiwi", "Mango", "Pear", "Pomegranate"};
    String[] options = {"Yes", "No"};
    private boolean isShowOtherForm = false;
    private String cropName = "";
    private boolean isPurchaseDocs = false;
    private boolean isDoc1 = false;
    private boolean isDoc2 = false;
    private String transactionClose = "0";
    private String reqCodeImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_master);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Create Master");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent() != null && getIntent().getExtras() != null) {
            masterId = getIntent().getStringExtra(Constants.MASTER_ID);
            isShowOtherForm = true;
        }

        radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                radioButton = findViewById(checkedId);
                if (radioButton.getText().toString().equals(getString(R.string.purchase))) {
                    llFarmer.setVisibility(View.VISIBLE);
                    llVendor.setVisibility(View.GONE);
                    isFarmer = true;
                    type = Constants.FARMER;
                } else if (radioButton.getText().toString().equals(getString(R.string.sell))) {
                    llFarmer.setVisibility(View.GONE);
                    llVendor.setVisibility(View.VISIBLE);
                    isFarmer = false;
                    type = Constants.VENDOR;
                }
                llCreateMaster.setVisibility(View.VISIBLE);
                radiogroup.setVisibility(View.GONE);
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, fruits);
        etCropName.setThreshold(1);
        etCropName.setAdapter(adapter);

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, options);
        etResidual.setThreshold(1);
        etResidual.setAdapter(adapter1);

        orderProductList = new ArrayList<>();
        invoiceProductList = new ArrayList<>();

        rvGrnProducts.setLayoutManager(new LinearLayoutManager(this));
        rvChallanProducts.setLayoutManager(new LinearLayoutManager(this));
        rvSInvoiceProducts.setLayoutManager(new LinearLayoutManager(this));
        rvPInvoiceProducts.setLayoutManager(new LinearLayoutManager(this));

        if (isShowOtherForm) {
            tvGrnForm.setVisibility(View.VISIBLE);
            tvPurchaseInvoice.setVisibility(View.VISIBLE);
            btnSendPaymentReq.setVisibility(View.VISIBLE);
            tvChallanForm.setVisibility(View.VISIBLE);
            tvSalesInvoice.setVisibility(View.VISIBLE);
            btnNewTransaction.setVisibility(View.VISIBLE);
        }

        if (!isEmpty(masterId)) {
            getDataById();
        }
    }

    public void getDataById() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Transaction> call = getService().getTransactionById(Integer.parseInt(masterId));
        call.enqueue(new Callback<Transaction>() {
            @Override
            public void onResponse(Call<Transaction> call, Response<Transaction> response) {
                Transaction transaction = response.body();
                if (checkStatus(transaction) && transaction.data != null) {
                    setData(transaction.data.get(0));
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Transaction> call, Throwable t) {
                failureError("get transaction failed");
            }
        });
    }

    private void setData(Transaction.Data transaction) {
        transactionClose = transaction.closeTransaction;
        etName.setText(transaction.name);
        etFarmName.setText(transaction.farmName);
        etMoNo.setText(transaction.contactNo);
        etVillage.setText(transaction.village);
        etTaluka.setText(transaction.taluka);
        etDistrict.setText(transaction.district);
        etAddress.setText(transaction.farmAddress);
        cropName = transaction.cropName;
        tvCropList.setText(cropName);
        tvVisitingCard.setHint(transaction.visitingCardImage);
        tvIdCard.setHint(transaction.idProofImage);
        tvPanCard.setText(transaction.panNo);
        tvPanCard.setHint(transaction.panImage);
        tvBankDetails.setHint(transaction.passbookImage);
        tvApmc.setText(transaction.apmcName);
        tvApmc.setHint(transaction.apmcImage);
        tvShopAct.setHint(transaction.shopActImage);
        tvShopAct.setText(transaction.shopAct);
        tvGst.setHint(transaction.gstImage);
        tvGst.setText(transaction.gstNo);
        tvIec.setHint(transaction.iecImage);
        tvIec.setText(transaction.iecNo);
        tvApeda.setHint(transaction.apedaImage);
        tvApeda.setText(transaction.apedaNo);
        etNoYears.setText(transaction.noOfYear);
        etNoPlants.setText(transaction.totalPlant);
        etLandUsed.setText(transaction.landForCrop);
        if (!transaction.eythrelDate.contains("0000-00-00") && !transaction.eythrelDate.contains("1970-01-01"))
            etEythrelDate.setText(decodeDate(transaction.eythrelDate));
        if (!transaction.wateringDate.contains("0000-00-00") && !transaction.wateringDate.contains("1970-01-01"))
            etWateringDate.setText(decodeDate(transaction.wateringDate));
        if (!transaction.expectedHarvestingDate.contains("0000-00-00") && !transaction.expectedHarvestingDate.contains("1970-01-01"))
            etHarvestingDate.setText(decodeDate(transaction.expectedHarvestingDate));
        etExpectedYield.setText(transaction.expectedYield);
        etResidual.setText(transaction.residualFreeNot);
        tvPhotosPlants.setHint(transaction.photoPlantImage);

        if (transaction.type.equals(Constants.FARMER)) {
            radioFarmer.setChecked(true);
            if (!TextUtils.isEmpty(transaction.orderComments))
                etFarmerComments2.setText(transaction.orderComments);
            if (!TextUtils.isEmpty(transaction.invComments))
                etFarmerComments3.setText(transaction.invComments);
            if (!isEmpty(transaction.document2)) {
                tvPDocs3.setText(transaction.document2);
            }
            if (!isEmpty(transaction.document3)) {
                tvPDocs3.setText(transaction.document3);
            }
            if (!transaction.orderNo.equals("0"))
                etOrderNo.setText(transaction.orderNo);
            if (!transaction.orderDate.contains("0000-00-00") && !transaction.orderDate.contains("1970-01-01"))
                etGrnDate.setText(decodeDate(transaction.orderDate));

            etVehicleNo.setText(transaction.vehicleNo);
            etDriverName.setText(transaction.driverName);
            etDriverMoNo.setText(transaction.driverMobile);
            etGoodsReceivedby.setText(transaction.goodsRecievedBy);
            etFarmerPaymentTerms.setText(transaction.paymentTearus);
            etGrnAuthorised.setText(transaction.authorizedBy);
            etPinvoiceAuthorised.setText(transaction.invAuthorizedBy);
            etPInvoiceNo.setText(transaction.invNo);
            if (transaction.invDate != null && !transaction.invDate.contains("0000-00-00")
                    && !transaction.invDate.contains("1970-01-01")) {
                etPDate.setText(decodeDate(transaction.invDate));
            }

            switch (transaction.sendRequest) {
                case "1":
                    btnSendPaymentReq.setText("Payment Request Sent");
                    btnSendPaymentReq.setEnabled(false);
                    btnCloseTransaction.setVisibility(View.GONE);
                    break;
                case "2":
                    btnSendPaymentReq.setVisibility(View.GONE);
                    btnCloseTransaction.setVisibility(View.VISIBLE);
                    break;
                case "3":
                    btnSendPaymentReq.setVisibility(View.GONE);
                    btnCloseTransaction.setVisibility(View.GONE);
                    break;
                default:
                    btnSendPaymentReq.setEnabled(true);
                    btnSendPaymentReq.setVisibility(View.VISIBLE);
                    btnCloseTransaction.setVisibility(View.GONE);
                    break;
            }

            switch (transaction.step) {
                case "10":
                    llCreateMaster.setVisibility(View.VISIBLE);
                    llGRN.setVisibility(View.GONE);
                    llPurchaseInvoice.setVisibility(View.GONE);
                    break;
                case "11":
                    llCreateMaster.setVisibility(View.GONE);
                    llGRN.setVisibility(View.VISIBLE);
                    llPurchaseInvoice.setVisibility(View.GONE);
                    break;
                case "21":
                    llCreateMaster.setVisibility(View.GONE);
                    llGRN.setVisibility(View.GONE);
                    llPurchaseInvoice.setVisibility(View.VISIBLE);
                    break;
                case "31":
                    llCreateMaster.setVisibility(View.GONE);
                    llGRN.setVisibility(View.GONE);
                    llPurchaseInvoice.setVisibility(View.GONE);
                    break;
                case "41":
                    llCreateMaster.setVisibility(View.GONE);
                    llGRN.setVisibility(View.GONE);
                    llPurchaseInvoice.setVisibility(View.GONE);
                    break;
                case "51":
                    llCreateMaster.setVisibility(View.GONE);
                    llGRN.setVisibility(View.GONE);
                    llPurchaseInvoice.setVisibility(View.GONE);
                    btnSendPaymentReq.setVisibility(View.GONE);
                    btnCloseTransaction.setVisibility(View.GONE);
                    btnFarmerFormSubmit.setVisibility(View.GONE);
                    btnGrnSubmit.setVisibility(View.GONE);
                    btnPurchaseInvoiceSubmit.setVisibility(View.GONE);
                    break;
            }
        } else {
            radioVendor.setChecked(true);

            if (!transaction.orderNo.equals("0"))
                etDeliveryNo.setText(transaction.orderNo);
            if (!transaction.orderDate.contains("0000-00-00") && !transaction.orderDate.contains("1970-01-01"))
                etChallanDate.setText(decodeDate(transaction.orderDate));

            if (!isEmpty(transaction.document2)) {
                tvSDocs2.setText(transaction.document2);
            }
            if (!isEmpty(transaction.document3)) {
                tvSDocs3.setText(transaction.document3);
            }

            if (!TextUtils.isEmpty(transaction.orderComments))
                etVendorComments2.setText(transaction.orderComments);
            if (!TextUtils.isEmpty(transaction.invComments))
                etVendorComments3.setText(transaction.invComments);

            etDispatchedBy.setText(transaction.disputedBy);
            etSVehicleNo.setText(transaction.vehicleNo);
            etSDriverName.setText(transaction.driverName);
            etSDriverMoNo.setText(transaction.driverMobile);
            etChallanAuthorised.setText(transaction.authorizedBy);
            etSinvoiceAuthorised.setText(transaction.invAuthorizedBy);
            if (!transaction.invNo.equals("0"))
                etSInvoiceNo.setText(transaction.invNo);
            if (!transaction.invDate.contains("0000-00-00") && !transaction.invDate.contains("1970-01-01"))
                etSDate.setText(decodeDate(transaction.invDate));
            etPlace.setText(transaction.placeOfSuppy);
            if (!transaction.deNo.equals("0"))
                etSDeliveryNo.setText(transaction.deNo);

            switch (transaction.step) {
                case "10":
                    llCreateMaster.setVisibility(View.VISIBLE);
                    llChallan.setVisibility(View.GONE);
                    llSalesInvoice.setVisibility(View.GONE);
                    break;
                case "11":
                    llCreateMaster.setVisibility(View.GONE);
                    llChallan.setVisibility(View.VISIBLE);
                    llSalesInvoice.setVisibility(View.GONE);
                    break;
                case "21":
                    llCreateMaster.setVisibility(View.GONE);
                    llChallan.setVisibility(View.GONE);
                    llSalesInvoice.setVisibility(View.VISIBLE);
                    break;
                case "31":
                    llCreateMaster.setVisibility(View.GONE);
                    llChallan.setVisibility(View.GONE);
                    llSalesInvoice.setVisibility(View.GONE);
                    btnChallanSubmit.setVisibility(View.GONE);
                    btnSalesInvoiceSubmit.setVisibility(View.GONE);
                    break;
            }
        }

        if (!isEmpty(transaction.orderProducts)) {
            ordersProducts = transaction.orderProducts;
            String[] split = transaction.orderProducts.split(",");
            for (String aSplit : split) {
                String[] splitTokens = aSplit.split("-");
                double total = Double.parseDouble(splitTokens[1]) * Double.parseDouble(splitTokens[2]);
                orderProductList.add(new Product(splitTokens[0], Double.parseDouble(splitTokens[2]), Double.parseDouble(splitTokens[1]), total));
            }
            double total = 0;
            for (int i = 0; i < orderProductList.size(); i++) {
                total = total + orderProductList.get(i).total;
            }

            if (isFarmer) {
                tvGRNTotal.setText("Total : " + total);
            } else {
                tvChallanTotal.setText("Total : " + total);
            }
            setOrderProductAdapter();
        }

        if (!isEmpty(transaction.invProducts)) {
            invProducts = transaction.invProducts;
            String[] split = transaction.invProducts.split(",");
            for (String aSplit : split) {
                String[] splitTokens = aSplit.split("-");
                double total = Double.parseDouble(splitTokens[1]) * Double.parseDouble(splitTokens[2]);
                invoiceProductList.add(new Product(splitTokens[0], Double.parseDouble(splitTokens[2]), Double.parseDouble(splitTokens[1]), total));
            }
            double total = 0;
            for (int i = 0; i < invoiceProductList.size(); i++) {
                total = total + invoiceProductList.get(i).total;
            }

            if (isFarmer) {
                tvPInvoiceTotal.setText("Total : " + total);
            } else {
                tvSInvoiceTotal.setText("Total : " + total);
            }
            setInvoiceProductAdapter();
        }
    }

    public void createMaster(final String step) {
        if (!isNetworkConnected())
            return;

        showProgress();

        MultipartBody.Part body2 = getBodyPart(documentFile2, "document2");
        MultipartBody.Part body3 = getBodyPart(documentFile3, "document3");
        MultipartBody.Part bodyVCard = getBodyPart(docVCard, "visiting_card_image");
        MultipartBody.Part bodyICard = getBodyPart(docICard, "id_proof_image");
        MultipartBody.Part bodyPanCard = getBodyPart(docPanCard, "pan_image");
        MultipartBody.Part bodyBanks = getBodyPart(docBank, "passbook_image");
        MultipartBody.Part bodyApmc = getBodyPart(docApmc, "apmc_image");
        MultipartBody.Part bodyShopAct = getBodyPart(docShopAct, "shop_act_image");
        MultipartBody.Part bodyGst = getBodyPart(docGst, "gst_image");
        MultipartBody.Part bodyIec = getBodyPart(docIec, "iec_image");
        MultipartBody.Part bodyApeda = getBodyPart(docApeda, "apeda_image");
        MultipartBody.Part bodyPhotoplant = getBodyPart(docPhotos, "photo_plant_image");

        RequestBody userType = RequestBody.create(MultipartBody.FORM, type);
        RequestBody stepCount = RequestBody.create(MultipartBody.FORM, step);
        RequestBody id = RequestBody.create(MultipartBody.FORM, masterId);
        RequestBody orderProducts = RequestBody.create(MultipartBody.FORM, ordersProducts);
        RequestBody invoiceProducts = RequestBody.create(MultipartBody.FORM, invProducts);

        // Deal Form params
        RequestBody name = RequestBody.create(MultipartBody.FORM, getName());
        RequestBody farmName = RequestBody.create(MultipartBody.FORM, getFarmName());
        RequestBody address = RequestBody.create(MultipartBody.FORM, getAddress());
        RequestBody mobileNo = RequestBody.create(MultipartBody.FORM, getMobileNo());
        RequestBody village = RequestBody.create(MultipartBody.FORM, getVillage());
        RequestBody taluka = RequestBody.create(MultipartBody.FORM, getTaluka());
        RequestBody district = RequestBody.create(MultipartBody.FORM, getDistrict());
        RequestBody pancardNo = RequestBody.create(MultipartBody.FORM, getPanCardNo());
        RequestBody apmcName = RequestBody.create(MultipartBody.FORM, getAPMC());
        RequestBody shopAct = RequestBody.create(MultipartBody.FORM, getShopAct());
        RequestBody gstNo = RequestBody.create(MultipartBody.FORM, getGST());
        RequestBody iecNo = RequestBody.create(MultipartBody.FORM, getIEC());
        RequestBody apedaNo = RequestBody.create(MultipartBody.FORM, getApeda());
        RequestBody noOfYear = RequestBody.create(MultipartBody.FORM, getNoOfYrs());
        RequestBody cropName = RequestBody.create(MultipartBody.FORM, getCropName());
        RequestBody totalPlant = RequestBody.create(MultipartBody.FORM, getTotalPlant());
        RequestBody landForCrop = RequestBody.create(MultipartBody.FORM, getLandForCrop());
        RequestBody eythrelDate = RequestBody.create(MultipartBody.FORM, getEthyralDate());
        RequestBody wateringDate = RequestBody.create(MultipartBody.FORM, getWateringDate());
        RequestBody expectedYield = RequestBody.create(MultipartBody.FORM, getExpectedYield());
        RequestBody harvestingDate = RequestBody.create(MultipartBody.FORM, getHarvestingDate());
        RequestBody residualFreeOrNot = RequestBody.create(MultipartBody.FORM, getResFreeOrNot());

        //Invoice
        RequestBody dispatchedBy = RequestBody.create(MultipartBody.FORM, getDispatchedBy());
        RequestBody goodReceivedBy = RequestBody.create(MultipartBody.FORM, getGoodsReceivedBy());
        RequestBody authorisedBy = RequestBody.create(MultipartBody.FORM, getOrderAuthorisedBy());
        RequestBody invAuthorisedBy = RequestBody.create(MultipartBody.FORM, getInvoiceAuthorisedBy());
        RequestBody paymentTerms = RequestBody.create(MultipartBody.FORM, getPaymentTerms());
        RequestBody invNo = RequestBody.create(MultipartBody.FORM, getInvoiceNo());
        RequestBody deNo = RequestBody.create(MultipartBody.FORM, getDeliveryNo());
        RequestBody placeOfSupply = RequestBody.create(MultipartBody.FORM, getPlaceOfSupply());
        RequestBody invDate = RequestBody.create(MultipartBody.FORM, getInvoiceDate());
        RequestBody comment2 = RequestBody.create(MultipartBody.FORM, getComment2());
        RequestBody comment3 = RequestBody.create(MultipartBody.FORM, getComment3());

        // GRN params
        RequestBody orderNo = RequestBody.create(MultipartBody.FORM, getOrderNo());
        RequestBody orderDate = RequestBody.create(MultipartBody.FORM, encodeDate(getOrderDate()));
        RequestBody vehicleNo = RequestBody.create(MultipartBody.FORM, getVehicleNo());
        RequestBody driverName = RequestBody.create(MultipartBody.FORM, getDriverName());
        RequestBody driverMoNo = RequestBody.create(MultipartBody.FORM, getDriverMoNo());

        String request = "0";

        if (step.equals("41")) {
            request = "1";
        }
        RequestBody paymentReq = RequestBody.create(MultipartBody.FORM, request);
        RequestBody closeTransaction = RequestBody.create(MultipartBody.FORM, transactionClose);

        RequestBody userId = RequestBody.create(MultipartBody.FORM, getUserId());

        Call<CreateMaster> call = getService().createMaster(userType, name, mobileNo, farmName, village, taluka, district, address,
                pancardNo, apmcName, shopAct, gstNo, iecNo, apedaNo, noOfYear, cropName, totalPlant, landForCrop, eythrelDate, wateringDate,
                expectedYield, harvestingDate, residualFreeOrNot, bodyVCard, bodyICard, bodyPanCard, bodyBanks, bodyApmc, bodyShopAct,
                bodyGst, bodyIec, bodyApeda, bodyPhotoplant, comment2, comment3, body2, body3, paymentTerms,
                orderProducts, orderNo, orderDate, dispatchedBy, vehicleNo, driverName, driverMoNo, goodReceivedBy, authorisedBy,
                invNo, deNo, placeOfSupply, invDate, invoiceProducts, invAuthorisedBy, stepCount, id, userId, paymentReq, closeTransaction);
        call.enqueue(new Callback<CreateMaster>() {
            @Override
            public void onResponse(Call<CreateMaster> call, Response<CreateMaster> response) {
                CreateMaster createMaster = response.body();
                if (checkStatus(createMaster)) {
                    masterId = String.valueOf(createMaster.id);
                    toastMessage("Update master successfully");
                    if (step.equals("41")) {
                        btnSendPaymentReq.setText("Payment Request Sent");
                        btnSendPaymentReq.setEnabled(false);
                    } else {
                        btnSendPaymentReq.setEnabled(true);
                    }

                    if (transactionClose.equals("1")) {
                        if (isFarmer) {
                            llCreateMaster.setVisibility(View.GONE);
                            llGRN.setVisibility(View.GONE);
                            llPurchaseInvoice.setVisibility(View.GONE);
                            btnSendPaymentReq.setVisibility(View.GONE);
                            btnCloseTransaction.setVisibility(View.GONE);
                            btnFarmerFormSubmit.setVisibility(View.GONE);
                            btnGrnSubmit.setVisibility(View.GONE);
                            btnPurchaseInvoiceSubmit.setVisibility(View.GONE);
                        }
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<CreateMaster> call, Throwable t) {
                failureError("update create master failed");
            }
        });
    }

    private MultipartBody.Part getBodyPart(File docPhotos, String imageName) {
        if (docPhotos != null && imageName != null) {
            Uri selectedUri = Uri.fromFile(docPhotos);
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());

            RequestBody requestFile = null;
            if (mimeType != null) {
                requestFile = RequestBody.create(MediaType.parse(mimeType), docPhotos);
            }

            if (requestFile != null) {
                return MultipartBody.Part.createFormData(imageName, docPhotos.getName(), requestFile);
            }
        }
        return null;
    }

    @OnClick({R.id.et_crop_name, R.id.btn_p_document2, R.id.btn_farmer_form_submit, R.id.et_grn_date, R.id.et_p_date,
            R.id.et_s_date, R.id.btn_grn_submit, R.id.btn_send_payment_req, R.id.btn_s_document2, R.id.btn_grn_product,
            R.id.btn_challan_product, R.id.btn_challan_submit, R.id.tv_grn_form, R.id.tv_challan_form, R.id.tv_purchase_invoice,
            R.id.btn_purchase_invoice_submit, R.id.btn_close_transaction, R.id.btn_sales_invoice_submit, R.id.tv_sales_invoice,
            R.id.img_add, R.id.et_challan_date, R.id.btn_p_invoice_product, R.id.btn_s_invoice_product, R.id.btn_p_document3,
            R.id.btn_s_document3, R.id.btn_new_transaction, R.id.btn_visiting_card, R.id.btn_id_card, R.id.btn_pan_card,
            R.id.btn_bank_details, R.id.btn_apmc, R.id.btn_shop_act, R.id.btn_gst, R.id.btn_iec, R.id.btn_apeda, R.id.btn_photos_plants,
            R.id.et_residual, R.id.et_eythrel_date, R.id.et_watering_date, R.id.et_harvesting_date})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.et_residual:
                etResidual.showDropDown();
                break;
            case R.id.btn_visiting_card:
                reqCodeImage = "btn_visiting_card";
                checkPermission();
                break;
            case R.id.btn_id_card:
                reqCodeImage = "btn_id_card";
                checkPermission();
                break;
            case R.id.btn_pan_card:
                reqCodeImage = "btn_pan_card";
                checkPermission();
                break;
            case R.id.btn_bank_details:
                reqCodeImage = "btn_bank_details";
                checkPermission();
                break;
            case R.id.btn_apmc:
                reqCodeImage = "btn_apmc";
                checkPermission();
                break;
            case R.id.btn_shop_act:
                reqCodeImage = "btn_shop_act";
                checkPermission();
                break;
            case R.id.btn_gst:
                reqCodeImage = "btn_gst";
                checkPermission();
                break;
            case R.id.btn_iec:
                reqCodeImage = "btn_iec";
                checkPermission();
                break;
            case R.id.btn_apeda:
                reqCodeImage = "btn_apeda";
                checkPermission();
                break;
            case R.id.btn_photos_plants:
                reqCodeImage = "btn_photos_plants";
                checkPermission();
                break;
            case R.id.btn_new_transaction:
                makeNewTransaction();
                break;
            case R.id.btn_p_document2:
                reqCodeImage = "btn_p_document2";
                checkPermission();
                break;
            case R.id.btn_p_document3:
                reqCodeImage = "btn_p_document3";
                checkPermission();
                break;
            case R.id.btn_s_document2:
                reqCodeImage = "btn_s_document2";
                checkPermission();
                break;
            case R.id.btn_s_document3:
                reqCodeImage = "btn_s_document3";
                checkPermission();
                break;
            case R.id.et_crop_name:
                etCropName.showDropDown();
                break;
            case R.id.et_eythrel_date:
                showDatePicker(etEythrelDate);
                break;
            case R.id.et_watering_date:
                showDatePicker(etWateringDate);
                break;
            case R.id.et_harvesting_date:
                showDatePicker(etHarvestingDate);
                break;
            case R.id.et_p_date:
                showDatePicker(etPDate);
                break;
            case R.id.et_s_date:
                showDatePicker(etSDate);
                break;
            case R.id.et_challan_date:
                showDatePicker(etChallanDate);
                break;
            case R.id.et_grn_date:
                showDatePicker(etGrnDate);
                break;
            case R.id.btn_farmer_form_submit:
                createMaster("11");
                break;
            case R.id.img_add:
                if (!isEmpty(getCropName())) {
                    if (isEmpty(cropName)) {
                        cropName = getCropName();
                    } else {
                        cropName = cropName + ", " + getCropName();
                    }
                }

                if (!isEmpty(cropName)) {
                    etCropName.setText("");
                    tvCropList.setText(cropName);
                    tvCropList.setVisibility(View.VISIBLE);
                } else {
                    tvCropList.setVisibility(View.GONE);
                }
                break;
            case R.id.btn_grn_submit:
                if (orderProductList != null && orderProductList.size() > 0) {
                    ordersProducts = "";
                    for (int i = 0; i < orderProductList.size(); i++) {
                        String singleProduct = orderProductList.get(i).name + "-" + orderProductList.get(i).rate + "-" + orderProductList.get(i).unit;
                        if (isEmpty(ordersProducts)) {
                            ordersProducts = singleProduct;
                        } else {
                            ordersProducts = ordersProducts + "," + singleProduct;
                        }
                    }
                }
                createMaster("21");
                break;
            case R.id.btn_challan_submit:
                if (orderProductList != null && orderProductList.size() > 0) {
                    ordersProducts = "";
                    for (int i = 0; i < orderProductList.size(); i++) {
                        String singleProduct = orderProductList.get(i).name + "-" + orderProductList.get(i).rate + "-" + orderProductList.get(i).unit;
                        if (isEmpty(ordersProducts)) {
                            ordersProducts = singleProduct;
                        } else {
                            ordersProducts = ordersProducts + "," + singleProduct;
                        }
                    }
                }
                createMaster("21");
                break;
            case R.id.btn_send_payment_req:
                createMaster("41");
                break;
            case R.id.btn_p_invoice_product:
            case R.id.btn_s_invoice_product:
                showAddProductDialog(true);
                break;
            case R.id.btn_grn_product:
            case R.id.btn_challan_product:
                showAddProductDialog(false);
                break;
            case R.id.tv_grn_form:
                if (llGRN.getVisibility() == View.VISIBLE)
                    llGRN.setVisibility(View.GONE);
                else
                    llGRN.setVisibility(View.VISIBLE);
                break;
            case R.id.tv_challan_form:
                if (llChallan.getVisibility() == View.VISIBLE)
                    llChallan.setVisibility(View.GONE);
                else
                    llChallan.setVisibility(View.VISIBLE);
                break;
            case R.id.tv_purchase_invoice:
                if (llPurchaseInvoice.getVisibility() == View.VISIBLE)
                    llPurchaseInvoice.setVisibility(View.GONE);
                else
                    llPurchaseInvoice.setVisibility(View.VISIBLE);
                break;
            case R.id.tv_sales_invoice:
                if (llSalesInvoice.getVisibility() == View.VISIBLE)
                    llSalesInvoice.setVisibility(View.GONE);
                else
                    llSalesInvoice.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_purchase_invoice_submit:
                if (invoiceProductList != null && invoiceProductList.size() > 0) {
                    invProducts = "";
                    for (int i = 0; i < invoiceProductList.size(); i++) {
                        String singleProduct = invoiceProductList.get(i).name + "-" + invoiceProductList.get(i).rate + "-" + invoiceProductList.get(i).unit;
                        if (isEmpty(invProducts)) {
                            invProducts = singleProduct;
                        } else {
                            invProducts = invProducts + "," + singleProduct;
                        }
                    }
                }
                createMaster("31");
                break;
            case R.id.btn_close_transaction:
                transactionClose = "1";
                createMaster("51");
                break;
            case R.id.btn_sales_invoice_submit:
                if (invoiceProductList != null && invoiceProductList.size() > 0) {
                    invProducts = "";
                    for (int i = 0; i < invoiceProductList.size(); i++) {
                        String singleProduct = invoiceProductList.get(i).name + "-" + invoiceProductList.get(i).rate + "-" + invoiceProductList.get(i).unit;
                        if (isEmpty(invProducts)) {
                            invProducts = singleProduct;
                        } else {
                            invProducts = invProducts + "," + singleProduct;
                        }
                    }
                }
                createMaster("31");
                break;
        }
    }

    public void makeNewTransaction() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().makeNewTransaction(masterId);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    finish();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("new transaction failed");
            }
        });
    }

    private String getName() {
        return etName.getText().toString().trim();
    }

    private String getFarmName() {
        return etFarmName.getText().toString().trim();
    }

    private String getVillage() {
        return etVillage.getText().toString().trim();
    }

    private String getDistrict() {
        return etDistrict.getText().toString().trim();
    }

    private String getTaluka() {
        return etTaluka.getText().toString().trim();
    }

    private String getAddress() {
        return etAddress.getText().toString().trim();
    }

    private String getMobileNo() {
        return etMoNo.getText().toString().trim();
    }

    private String getCropName() {
        return etCropName.getText().toString().trim();
    }

    private String getPanCardNo() {
        return tvPanCard.getText().toString().trim();
    }

    private String getAPMC() {
        return tvApmc.getText().toString().trim();
    }

    private String getShopAct() {
        return tvShopAct.getText().toString().trim();
    }

    private String getGST() {
        return tvGst.getText().toString().trim();
    }

    private String getIEC() {
        return tvIec.getText().toString().trim();
    }

    private String getApeda() {
        return tvApeda.getText().toString().trim();
    }

    private String getNoOfYrs() {
        return etNoYears.getText().toString().trim();
    }

    private String getTotalPlant() {
        return etNoPlants.getText().toString().trim();
    }

    private String getLandForCrop() {
        return etLandUsed.getText().toString().trim();
    }

    private String getEthyralDate() {
        return etEythrelDate.getText().toString().trim();
    }

    private String getWateringDate() {
        return etWateringDate.getText().toString().trim();
    }

    private String getExpectedYield() {
        return etExpectedYield.getText().toString().trim();
    }

    private String getHarvestingDate() {
        return etHarvestingDate.getText().toString().trim();
    }

    private String getResFreeOrNot() {
        return etResidual.getText().toString().trim();
    }

    private String getComment2() {
        return isFarmer ? etFarmerComments2.getText().toString().trim() : etVendorComments2.getText().toString().trim();
    }

    private String getComment3() {
        return isFarmer ? etFarmerComments3.getText().toString().trim() : etVendorComments3.getText().toString().trim();
    }

    private String getPaymentTerms() {
        return etFarmerPaymentTerms.getText().toString().trim();
    }

    private String getOrderNo() {
        return isFarmer ? etOrderNo.getText().toString().trim() : etDeliveryNo.getText().toString().trim();
    }

    private String getOrderDate() {
        return isFarmer ? etGrnDate.getText().toString().trim() : etChallanDate.getText().toString().trim();
    }

    private String getDispatchedBy() {
        return etDispatchedBy.getText().toString().trim();
    }

    private String getVehicleNo() {
        return isFarmer ? etVehicleNo.getText().toString().trim() : etSVehicleNo.getText().toString().trim();
    }

    private String getDriverName() {
        return isFarmer ? etDriverName.getText().toString().trim() : etSDriverName.getText().toString().trim();
    }

    private String getDriverMoNo() {
        return isFarmer ? etDriverMoNo.getText().toString().trim() : etSDriverMoNo.getText().toString().trim();
    }

    private String getOrderAuthorisedBy() {
        return isFarmer ? etGrnAuthorised.getText().toString().trim() : etChallanAuthorised.getText().toString().trim();
    }

    private String getInvoiceAuthorisedBy() {
        return isFarmer ? etPinvoiceAuthorised.getText().toString().trim() : etSinvoiceAuthorised.getText().toString().trim();
    }

    private String getInvoiceNo() {
        return isFarmer ? etPInvoiceNo.getText().toString().trim() : etSInvoiceNo.getText().toString().trim();
    }

    private String getGoodsReceivedBy() {
        return etGoodsReceivedby.getText().toString().trim();
    }

    private String getPlaceOfSupply() {
        return etPlace.getText().toString().trim();
    }

    private String getDeliveryNo() {
        return etSDeliveryNo.getText().toString().trim();
    }

    private String getInvoiceDate() {
        return isFarmer ? etPDate.getText().toString().trim() : etSDate.getText().toString().trim();
    }

    public void showAddProductDialog(final boolean isInvoice) {
        final Dialog dialog = new Dialog(this);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_add_product);
        dialog.setCancelable(true);

        final AutoCompleteTextView etProduct = dialog.findViewById(R.id.et_product);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, fruits);
        etProduct.setThreshold(1);
        etProduct.setAdapter(adapter);

        final EditText etUnit = dialog.findViewById(R.id.et_unit);
        final EditText etRate = dialog.findViewById(R.id.et_rate);
        final TextView tvTotal = dialog.findViewById(R.id.tv_total);
        TextView tvCancel = dialog.findViewById(R.id.btn_cancel);
        TextView tvAdd = dialog.findViewById(R.id.btn_add);

        etProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etProduct.showDropDown();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        etRate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isEmpty(etRate.getText().toString())) {
                    tvTotal.setText("");
                    return;
                }
                if (isEmpty(etUnit.getText().toString())) {
                    tvTotal.setText("");
                    return;
                }

                double total = Double.parseDouble(etRate.getText().toString()) * Double.parseDouble(etUnit.getText().toString());
                tvTotal.setText("Total : " + total);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        etUnit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isEmpty(etUnit.getText().toString())) {
                    tvTotal.setText("");
                    return;
                }
                if (isEmpty(etRate.getText().toString())) {
                    tvTotal.setText("");
                    return;
                }

                double total = Double.parseDouble(etRate.getText().toString()) * Double.parseDouble(etUnit.getText().toString());
                tvTotal.setText("Total : " + total);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String product = etProduct.getText().toString().trim();
                String unit = etUnit.getText().toString().trim();
                String rate = etRate.getText().toString().trim();

                if (isEmpty(product)) {
                    validationError("Please enter Product");
                    return;
                }

                if (isEmpty(unit)) {
                    validationError("Please enter unit");
                    return;
                }

                if (isEmpty(rate)) {
                    validationError("Please enter rate");
                    return;
                }

                double total = Double.parseDouble(rate) * Double.parseDouble(unit);
                if (isInvoice) {
                    invoiceProductList.add(new Product(product, Double.parseDouble(unit), Double.parseDouble(rate), total));
                } else {
                    orderProductList.add(new Product(product, Double.parseDouble(unit), Double.parseDouble(rate), total));
                }
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;
        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (isInvoice) {
                    double total = 0;
                    for (int i = 0; i < invoiceProductList.size(); i++) {
                        total = total + invoiceProductList.get(i).total;
                    }

                    if (isFarmer) {
                        tvPInvoiceTotal.setText("Total : " + total);
                    } else {
                        tvSInvoiceTotal.setText("Total : " + total);
                    }
                    setInvoiceProductAdapter();
                } else {
                    double total = 0;
                    for (int i = 0; i < orderProductList.size(); i++) {
                        total = total + orderProductList.get(i).total;
                    }

                    if (isFarmer) {
                        tvGRNTotal.setText("Total : " + total);
                    } else {
                        tvChallanTotal.setText("Total : " + total);
                    }

                    setOrderProductAdapter();
                }
//                if (isFarmer) {
//                    if (isInvoice) {
//                        double total = 0;
//                        for (int i = 0; i < invoiceProductList.size(); i++) {
//                            total = total + invoiceProductList.get(i).total;
//                        }
//
//                        tvPInvoiceTotal.setText("Total : " + total);
//                        setInvoiceProductAdapter();
//                    } else {
//                        double total = 0;
//                        for (int i = 0; i < orderProductList.size(); i++) {
//                            total = total + orderProductList.get(i).total;
//                        }
//
//                        tvGRNTotal.setText("Total : " + total);
//                        setOrderProductAdapter();
//                    }
//                } else {
//                    double total = 0;
//                    for (int i = 0; i < invoiceProductList.size(); i++) {
//                        total = total + invoiceProductList.get(i).total;
//                    }
//
//                    tvChallanTotal.setText("Total : " + total);
//                    setInvoiceProductAdapter();
//                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    private void showDatePicker(final EditText editText) {
        Calendar myCalendar = Calendar.getInstance();

        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                cal.set(Calendar.MONTH, month);
                String date = new SimpleDateFormat("d MMM, yyyy").format(cal.getTime());
                editText.setText(date);
            }
        }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void checkPermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            CropImage.activity()
                                    .setGuidelines(CropImageView.Guidelines.ON)
                                    .start(CreateMasterActivity.this);
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            toastMessage("Please give permission");
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                switch (reqCodeImage) {
                    case "btn_s_document3":
                        documentFile3 = new File(resultUri.getPath());
                        tvSDocs3.setText(documentFile3.getName());
                        break;
                    case "btn_s_document2":
                        documentFile2 = new File(resultUri.getPath());
                        tvSDocs2.setText(documentFile2.getName());
                        break;
                    case "btn_p_document2":
                        documentFile2 = new File(resultUri.getPath());
                        tvPDocs2.setText(documentFile2.getName());
                        break;
                    case "btn_p_document3":
                        documentFile3 = new File(resultUri.getPath());
                        tvPDocs3.setText(documentFile3.getName());
                        break;
                    case "btn_visiting_card":
                        docVCard = new File(resultUri.getPath());
                        tvVisitingCard.setHint(docVCard.getName());
                        break;
                    case "btn_id_card":
                        docICard = new File(resultUri.getPath());
                        tvIdCard.setHint(docICard.getName());
                        break;
                    case "btn_pan_card":
                        docPanCard = new File(resultUri.getPath());
                        tvPanCard.setHint(docPanCard.getName());
                        break;
                    case "btn_bank_details":
                        docBank = new File(resultUri.getPath());
                        tvBankDetails.setHint(docBank.getName());
                        break;
                    case "btn_apmc":
                        docApmc = new File(resultUri.getPath());
                        tvApmc.setText(docApmc.getName());
                        break;
                    case "btn_shop_act":
                        docShopAct = new File(resultUri.getPath());
                        tvShopAct.setHint(docShopAct.getName());
                        break;
                    case "btn_gst":
                        docGst = new File(resultUri.getPath());
                        tvGst.setHint(docGst.getName());
                        break;
                    case "btn_iec":
                        docIec = new File(resultUri.getPath());
                        tvIec.setHint(docIec.getName());
                        break;
                    case "btn_apeda":
                        docApeda = new File(resultUri.getPath());
                        tvApeda.setHint(docApeda.getName());
                        break;
                    case "btn_photos_plants":
                        docPhotos = new File(resultUri.getPath());
                        tvPhotosPlants.setHint(docPhotos.getName());
                        break;
                }
            }
        }
    }

    private void setOrderProductAdapter() {
        if (orderProductList != null && orderProductList.size() > 0) {
            if (orderAdapter == null) {
                orderAdapter = new ProductsAdapter(this);
            }
            orderAdapter.doRefresh(orderProductList);
            if (isFarmer) {
                if (rvGrnProducts.getAdapter() == null) {
                    rvGrnProducts.setAdapter(orderAdapter);
                }
                llGrnProduct.setVisibility(View.VISIBLE);
            } else {
                if (rvChallanProducts.getAdapter() == null) {
                    rvChallanProducts.setAdapter(orderAdapter);
                }
                llChallanProduct.setVisibility(View.VISIBLE);
            }
        } else {
            if (isFarmer)
                llGrnProduct.setVisibility(View.GONE);
            else
                llChallanProduct.setVisibility(View.GONE);
        }
    }

    private void setInvoiceProductAdapter() {
        if (invoiceProductList != null && invoiceProductList.size() > 0) {
            if (invoiceAdapter == null) {
                invoiceAdapter = new ProductsAdapter(this);
            }
            invoiceAdapter.doRefresh(invoiceProductList);
            if (isFarmer) {
                if (rvPInvoiceProducts.getAdapter() == null) {
                    rvPInvoiceProducts.setAdapter(invoiceAdapter);
                }
                llPInvoiceProduct.setVisibility(View.VISIBLE);
            } else {
                if (rvSInvoiceProducts.getAdapter() == null) {
                    rvSInvoiceProducts.setAdapter(invoiceAdapter);
                }
                llSInvoiceProduct.setVisibility(View.VISIBLE);
            }
        } else {
            if (isFarmer)
                llPInvoiceProduct.setVisibility(View.GONE);
            else
                llSInvoiceProduct.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //createMaster("");
        super.onBackPressed();
    }

    private String encodeDate(String date) {
        return Utils.changeDateFormat("d MMM, yyyy", "yyyy-MM-dd hh:mm:ss", date);
    }

    private String decodeDate(String date) {
        return Utils.changeDateFormat("yyyy-MM-dd hh:mm:ss", "d MMM, yyyy", date);
    }
}
