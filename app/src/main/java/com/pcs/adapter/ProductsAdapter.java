package com.pcs.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pcs.R;
import com.pcs.model.Product;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<Product> mData;

    public ProductsAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_product_list, parent, false);
        return new MyViewHolder(view);
    }

    public void doRefresh(ArrayList<Product> products) {
        this.mData = products;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Product item = mData.get(position);

        holder.tvSrNo.setText((position + 1) + "");
        holder.tvProduct.setText(item.name);
        holder.tvUnit.setText(item.unit + "");
        holder.tvRate.setText(item.rate + "");
        holder.tvTotal.setText(item.total + "");
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_sr_no)
        TextView tvSrNo;
        @BindView(R.id.tv_product)
        TextView tvProduct;
        @BindView(R.id.tv_unit)
        TextView tvUnit;
        @BindView(R.id.tv_rate)
        TextView tvRate;
        @BindView(R.id.tv_total)
        TextView tvTotal;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}