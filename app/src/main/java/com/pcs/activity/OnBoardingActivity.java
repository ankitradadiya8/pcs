package com.pcs.activity;

import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.textview.TextViewSFTextMedium;
import android.view.View;

import com.pcs.R;
import com.pcs.adapter.OnBoardingAdapter;
import com.pcs.model.Transaction;
import com.pcs.util.EqualSpacingItemDecoration;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnBoardingActivity extends BaseActivity {

    @BindView(R.id.rv_onboarding)
    RecyclerView rvOnboarding;
    @BindView(R.id.btn_add)
    FloatingActionButton btnAdd;
    @BindView(R.id.tv_no_data)
    TextViewSFTextMedium tvNoData;

    private OnBoardingAdapter mAdapter;
    private ArrayList<Transaction.Data> transactionArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("On Board");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvOnboarding.setLayoutManager(new LinearLayoutManager(this));
        rvOnboarding.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.VERTICAL));
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAllBoardData();
    }

    public void getAllBoardData() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Transaction> call = getService().getOnBoardData(getUserId());
        call.enqueue(new Callback<Transaction>() {
            @Override
            public void onResponse(Call<Transaction> call, Response<Transaction> response) {
                Transaction transaction = response.body();
                if (checkStatus(transaction)) {
                    transactionArrayList = (ArrayList<Transaction.Data>) transaction.data;
                    setAdapter();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Transaction> call, Throwable t) {
                failureError("get OnBoard failed");
            }
        });
    }

    private void setAdapter() {
        if (transactionArrayList != null && transactionArrayList.size() > 0) {
            tvNoData.setVisibility(View.GONE);
            if (mAdapter == null) {
                mAdapter = new OnBoardingAdapter(this);
            }
            mAdapter.doRefresh(transactionArrayList);
            if (rvOnboarding.getAdapter() == null) {
                rvOnboarding.setAdapter(mAdapter);
            }
        } else {
            tvNoData.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btn_add)
    public void onViewClicked() {
        redirectActivity(AddOnBoardingActivity.class);
    }
}
