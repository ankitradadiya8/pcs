package com.pcs.api;

import com.pcs.model.CreateMaster;
import com.pcs.model.GeneralModel;
import com.pcs.model.Outstanding;
import com.pcs.model.Plans;
import com.pcs.model.Transaction;
import com.pcs.util.Constants;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    @FormUrlEncoded
    @POST(Constants.LOGIN)
    Call<Object> login(@Field("email") String email, @Field("password") String password, @Field("device_token") String device_token);

    @FormUrlEncoded
    @POST(Constants.SIGNUP)
    Call<Object> register(@Field("email") String email, @Field("username") String username,
                          @Field("password") String password, @Field("device_token") String device_token,
                          @Field("mobile_no") String mobile_no, @Field("mobile_alter") String mobile_alter,
                          @Field("address") String address, @Field("pan_no") String pan_no,
                          @Field("adhar_no") String adhar_no, @Field("comments") String comments,
                          @Field("bank_name") String bank_name, @Field("account_no") String account_no,
                          @Field("ifsc") String ifsc, @Field("micr") String micr);

    @FormUrlEncoded
    @POST(Constants.ADD_NOTE)
    Call<GeneralModel> addPlan(@Field("user_id") String user_id, @Field("message") String message,
                               @Field("datetime") String datetime);

    @FormUrlEncoded
    @POST(Constants.TRANSACTION)
    Call<Transaction> getAllTransaction(@Field("user_id") String user_id, @Field("type") String type, @Field("status") int status);

    @FormUrlEncoded
    @POST(Constants.TRANSACTION)
    Call<Transaction> getPendingTransaction(@Field("status") int status, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constants.TRANSACTION)
    Call<Transaction> getTransactionById(@Field("id") int id);

    @FormUrlEncoded
    @POST(Constants.SEARCH_TRANSACTION)
    Call<Object> getSearchList(@Field("name") String name, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constants.NEW_TRANSACTION)
    Call<GeneralModel> makeNewTransaction(@Field("tid") String tid);

    @FormUrlEncoded
    @POST(Constants.ADD_HELP)
    Call<GeneralModel> addHelp(@Field("help") String help, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constants.GET_PLANS)
    Call<Plans> getPlans(@Field("user_id") String user_id);

    @Multipart
    @POST(Constants.CREATE_MASTER)
    Call<CreateMaster> createMaster(@Part("type") RequestBody type, @Part("name") RequestBody name,
                                    @Part("contact_no") RequestBody contact_no, @Part("farm_name") RequestBody farm_name,
                                    @Part("village") RequestBody village, @Part("taluka") RequestBody taluka,
                                    @Part("district") RequestBody district, @Part("farm_address") RequestBody farm_address,
                                    @Part("pan_no") RequestBody pan_no, @Part("apmc_name") RequestBody apmc_name,
                                    @Part("shop_act") RequestBody shop_act, @Part("gst_no") RequestBody gst_no,
                                    @Part("iec_no") RequestBody iec_no, @Part("apeda_no") RequestBody apeda_no,
                                    @Part("no_of_year") RequestBody no_of_year, @Part("crop_name") RequestBody crop_name,
                                    @Part("total_plant") RequestBody total_plant, @Part("land_for_crop") RequestBody land_for_crop,
                                    @Part("eythrel_date") RequestBody eythrel_date, @Part("watering_date") RequestBody watering_date,
                                    @Part("expected_yield") RequestBody expected_yield, @Part("expected_harvesting_date") RequestBody expected_harvesting_date,
                                    @Part("residual_free_not") RequestBody residual_free_not, @Part MultipartBody.Part docVCard,
                                    @Part MultipartBody.Part docICard, @Part MultipartBody.Part docPanCard,
                                    @Part MultipartBody.Part docBank, @Part MultipartBody.Part docApmc, @Part MultipartBody.Part docShopAct,
                                    @Part MultipartBody.Part docGST, @Part MultipartBody.Part docIEC, @Part MultipartBody.Part docApeda,
                                    @Part MultipartBody.Part docPhotoPlant, @Part("order_comments") RequestBody comment2,
                                    @Part("inv_comments") RequestBody comment3, @Part MultipartBody.Part documents2,
                                    @Part MultipartBody.Part documents3,
                                    @Part("payment_tearus") RequestBody payment_terms, @Part("order_products") RequestBody orderProducts,
                                    @Part("order_no") RequestBody order_no, @Part("order_date") RequestBody order_date,
                                    @Part("disputed_by") RequestBody dispatchedBy, @Part("vehicle_no") RequestBody vehicle_no,
                                    @Part("driver_name") RequestBody driver_name, @Part("driver_mobile") RequestBody driver_mobile,
                                    @Part("goods_recieved_by") RequestBody goodsRecievedBy, @Part("authorized_by") RequestBody authorizedBy,
                                    @Part("inv_no") RequestBody inv_no, @Part("de_no") RequestBody de_no,
                                    @Part("place_of_suppy") RequestBody place_of_supply, @Part("inv_date") RequestBody inv_date,
                                    @Part("inv_products") RequestBody invProducts, @Part("inv_authorized_by") RequestBody invAuthorizedBy,
                                    @Part("step") RequestBody step, @Part("id") RequestBody id, @Part("user_id") RequestBody user_id,
                                    @Part("send_request") RequestBody send_request, @Part("close_transaction") RequestBody closeTransaction);

    @FormUrlEncoded
    @POST(Constants.GET_ON_BOARD)
    Call<Transaction> getOnBoardData(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constants.VIEW_COLLECTION)
    Call<Outstanding> getCollection(@Field("user_id") String user_id);

    @Multipart
    @POST(Constants.ADD_ON_BOARD)
    Call<CreateMaster> addOnBoard(@Part("name") RequestBody name,
                                  @Part("contact_no") RequestBody contact_no, @Part("farm_name") RequestBody farm_name,
                                  @Part("village") RequestBody village, @Part("taluka") RequestBody taluka,
                                  @Part("district") RequestBody district, @Part("farm_address") RequestBody farm_address,
                                  @Part("pan_no") RequestBody pan_no, @Part("apmc_name") RequestBody apmc_name,
                                  @Part("shop_act") RequestBody shop_act, @Part("gst_no") RequestBody gst_no,
                                  @Part("iec_no") RequestBody iec_no, @Part("apeda_no") RequestBody apeda_no,
                                  @Part("no_of_year") RequestBody no_of_year, @Part("crop_name") RequestBody crop_name,
                                  @Part("total_plant") RequestBody total_plant, @Part("land_for_crop") RequestBody land_for_crop,
                                  @Part("eythrel_date") RequestBody eythrel_date, @Part("watering_date") RequestBody watering_date,
                                  @Part("expected_yield") RequestBody expected_yield, @Part("expected_harvesting_date") RequestBody expected_harvesting_date,
                                  @Part("residual_free_not") RequestBody residual_free_not, @Part MultipartBody.Part docVCard,
                                  @Part MultipartBody.Part docICard, @Part MultipartBody.Part docPanCard,
                                  @Part MultipartBody.Part docBank, @Part MultipartBody.Part docApmc, @Part MultipartBody.Part docShopAct,
                                  @Part MultipartBody.Part docGST, @Part MultipartBody.Part docIEC, @Part MultipartBody.Part docApeda,
                                  @Part MultipartBody.Part docPhotoPlant, @Part("user_id") RequestBody user_id,
                                  @Part("id") RequestBody id);

//    @FormUrlEncoded
//    @POST(Constants.ADD_ON_BOARD)
//    Call<CreateMaster> addOnBoard(@Field("name") String name, @Field("address") String address,
//                                  @Field("mobile_no") String mobile_no, @Field("mobile_alter") String mobile_alter,
//                                  @Field("pan_no") String pan_no, @Field("adhar_no") String adhar_no,
//                                  @Field("product") String product, @Field("comments") String comments,
//                                  @Field("product_rate") String product_rate, @Field("harvesting_date") String harvesting_date,
//                                  @Field("expected_total") String expected_total, @Field("harvesting_per_day") String harvesting_per_day,
//                                  @Field("payment_tearus") String payment_terms, @Field("representative") String representative,
//                                  @Field("user_id") String user_id, @Field("id") String id);
}
