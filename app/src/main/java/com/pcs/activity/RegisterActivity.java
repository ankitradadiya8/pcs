package com.pcs.activity;

import android.button.ButtonSFTextSemiBold;
import android.edittext.EditTextSFTextRegular;
import android.os.Bundle;

import com.google.gson.Gson;
import com.pcs.R;
import com.pcs.model.UserModel;
import com.pcs.util.Constants;
import com.pcs.util.Preferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends BaseActivity {

    @BindView(R.id.et_name)
    EditTextSFTextRegular etName;
    @BindView(R.id.et_password)
    EditTextSFTextRegular etPassword;
    @BindView(R.id.et_mobile_no)
    EditTextSFTextRegular etMobileNo;
    @BindView(R.id.et_alt_mobile_no)
    EditTextSFTextRegular etAltMobileNo;
    @BindView(R.id.et_address)
    EditTextSFTextRegular etAddress;
    @BindView(R.id.et_pan_no)
    EditTextSFTextRegular etPanNo;
    @BindView(R.id.et_aadhar_no)
    EditTextSFTextRegular etAadharNo;
    @BindView(R.id.et_comments)
    EditTextSFTextRegular etComments;
    @BindView(R.id.et_account_no)
    EditTextSFTextRegular etAccountNo;
    @BindView(R.id.et_bank)
    EditTextSFTextRegular etBank;
    @BindView(R.id.et_ifsc)
    EditTextSFTextRegular etIfsc;
    @BindView(R.id.et_micr)
    EditTextSFTextRegular etMicr;
    @BindView(R.id.btn_register)
    ButtonSFTextSemiBold btnRegister;
    @BindView(R.id.et_email)
    EditTextSFTextRegular etEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Sign Up");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.btn_register)
    public void onViewClicked() {
        if (isValid()) {
            register();
        }
    }

    public void register() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Object> call = getService().register(getEmail(), getName(), getPassword(), getDeviceToken(),
                getMobileNo(), getAltMobileNo(), getAddress(), getPanCardNo(), getAadharCardNo(), getComment(),
                getBankName(), getAccountNo(), getIFSCCode(), getMICRNo());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String json = new Gson().toJson(response.body());
                if (checkStatus(json)) {
                    UserModel userModel = new Gson().fromJson(json, UserModel.class);
                    Preferences.saveUserData(RegisterActivity.this, userModel.data.get(0));
                    Preferences.writeBoolean(RegisterActivity.this, Constants.IS_LOGIN, true);
                    gotoMainActivity();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("register failed");
            }
        });
    }

    private boolean isValid() {
        if (isEmpty(getName())) {
            validationError("Please enter name");
            return false;
        }

        if (!isValidEmail(getEmail())) {
            validationError("Please enter valid email");
            return false;
        }

//        if (isEmpty(getPassword())) {
//            validationError("Please enter password");
//            return false;
//        }

        if (isEmpty(getAddress())) {
            validationError("Please enter address");
            return false;
        }

        if (isEmpty(getMobileNo())) {
            validationError("Please enter mobile no");
            return false;
        }

//        if (isEmpty(getPanCardNo())) {
//            validationError("Please enter pan card no");
//            return false;
//        }
//
//        if (isEmpty(getAadharCardNo())) {
//            validationError("Please enter aadhar card no");
//            return false;
//        }
        return true;
    }

    private String getEmail() {
        return etEmail.getText().toString().trim();
    }

    private String getName() {
        return etName.getText().toString().trim();
    }

    private String getPassword() {
        return etPassword.getText().toString().trim();
    }

    private String getAddress() {
        return etAddress.getText().toString().trim();
    }

    private String getMobileNo() {
        return etMobileNo.getText().toString().trim();
    }

    private String getAltMobileNo() {
        return etAltMobileNo.getText().toString().trim();
    }

    private String getPanCardNo() {
        return etPanNo.getText().toString().trim();
    }

    private String getAadharCardNo() {
        return etAadharNo.getText().toString().trim();
    }

    private String getComment() {
        return etComments.getText().toString().trim();
    }

    private String getBankName() {
        return etBank.getText().toString().trim();
    }

    private String getAccountNo() {
        return etAccountNo.getText().toString().trim();
    }

    private String getIFSCCode() {
        return etIfsc.getText().toString().trim();
    }

    private String getMICRNo() {
        return etMicr.getText().toString().trim();
    }
}
