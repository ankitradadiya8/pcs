package com.pcs.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.textview.TextViewSFTextRegular;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pcs.R;
import com.pcs.activity.BaseActivity;
import com.pcs.activity.CARActivity;
import com.pcs.activity.CreateMasterActivity;
import com.pcs.activity.TransactionActivity;
import com.pcs.model.GeneralModel;
import com.pcs.model.Transaction;
import com.pcs.util.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.MyViewHolder> implements Filterable {

    private Context mContext;
    private List<Transaction.Data> mData;
    private List<Transaction.Data> mDataListFiltered;
    private boolean isCAR = false;
    private boolean isSearchData = false;
    private BaseActivity activity;

    public TransactionAdapter(Context mContext) {
        this.mContext = mContext;
        activity = (BaseActivity) mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_pending_task, parent, false);
        return new MyViewHolder(view);
    }

    public void setCAR(boolean isCAR) {
        this.isCAR = isCAR;
    }

    public void setSearch(boolean isSearchData) {
        this.isSearchData = isSearchData;
    }

    public void doRefresh(ArrayList<Transaction.Data> arrayList) {
        this.mData = arrayList;
        this.mDataListFiltered = arrayList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Transaction.Data item = mDataListFiltered.get(position);
        holder.tvName.setText(item.name);
        if (item.status.equals("0")) {
            holder.rlView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.pending_task_bg));
            holder.tvStatus.setText("Pending");
            holder.tvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red));
        } else {
            holder.rlView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.completed_task_bg));
            holder.tvStatus.setText("Completed");
            holder.tvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }
        holder.tvInvoice.setText("Invoice no : " + item.invNo);
        if (item.type.equals(Constants.FARMER))
            holder.tvType.setText(mContext.getString(R.string.purchase));
        else {
            holder.tvType.setText(mContext.getString(R.string.sell));
        }
    }

    @Override
    public int getItemCount() {
        return mDataListFiltered != null ? mDataListFiltered.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextViewSFTextRegular tvName;
        @BindView(R.id.tv_type)
        TextViewSFTextRegular tvType;
        @BindView(R.id.tv_status)
        TextViewSFTextRegular tvStatus;
        @BindView(R.id.rl_view)
        RelativeLayout rlView;
        @BindView(R.id.tv_invoice)
        TextViewSFTextRegular tvInvoice;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isSearchData) {
                        showNewTransactionDialog(mDataListFiltered.get(getAdapterPosition()).id);
                    } else {
                        Intent i = new Intent(mContext, isCAR ? CARActivity.class : CreateMasterActivity.class);
                        i.putExtra(Constants.MASTER_ID, mDataListFiltered.get(getAdapterPosition()).id);
                        mContext.startActivity(i);
                    }
                }
            });
        }
    }

    private void showNewTransactionDialog(final String id) {
        final Dialog dialog = new Dialog(mContext, R.style.Theme_AppCompat_Dialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.setCancelable(true);

        TextView tvMessage = dialog.findViewById(R.id.tv_message);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvChatnow = dialog.findViewById(R.id.tv_yes);
        tvMessage.setText("Create new transaction?");

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvChatnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                makeNewTransaction(id);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void makeNewTransaction(String id) {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<GeneralModel> call = activity.getService().makeNewTransaction(id);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (activity.checkStatus(response.body())) {
                    ((TransactionActivity) activity).refreshList();
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                activity.failureError("new transaction failed");
            }
        });
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mDataListFiltered = mData;
                } else {
                    List<Transaction.Data> filteredList = new ArrayList<>();
                    for (Transaction.Data row : mData) {
                        if (row.name.toLowerCase().contains(charString.toLowerCase())
                                || row.farmAddress.toLowerCase().contains(charSequence.toString())
                                || row.invProducts.toLowerCase().contains(charSequence.toString())) {
                            filteredList.add(row);
                        }
                    }

                    mDataListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mDataListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mDataListFiltered = (ArrayList<Transaction.Data>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}