package com.pcs.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.widget.DatePicker;

import com.pcs.R;
import com.pcs.adapter.MyPlanAdapter;
import com.pcs.model.Plans;
import com.pcs.util.Constants;
import com.pcs.util.EqualSpacingItemDecoration;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPlanActivity extends BaseActivity {

    @BindView(R.id.rv_plans)
    RecyclerView rvPlans;
    @BindView(R.id.datepicker)
    DatePicker datepicker;
    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;

    private MyPlanAdapter mAdapter;
    private ArrayList<Plans.Data> plansArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_plan);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("My Plans");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        plansArrayList = new ArrayList<>();

        datepicker.setMinDate(System.currentTimeMillis() - 1000);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        datepicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH),
                new DatePicker.OnDateChangedListener() {

                    @Override
                    public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                        Log.e("Date", "Year=" + year + " Month=" + (month + 1) + " day=" + dayOfMonth);
                        Calendar cal = Calendar.getInstance();
                        cal.set(Calendar.YEAR, year);
                        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        cal.set(Calendar.MONTH, month);
                        String date = new SimpleDateFormat("d MMM, yyyy").format(cal.getTime());
                        Intent i = new Intent(MyPlanActivity.this, AddPlanActivity.class);
                        i.putExtra(Constants.PLAN_DATE, date);
                        startActivity(i);
                    }
                });

        rvPlans.setLayoutManager(new LinearLayoutManager(this));
        rvPlans.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.VERTICAL));
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPlans();
    }

    public void getPlans() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Plans> call = getService().getPlans(getUserId());
        call.enqueue(new Callback<Plans>() {
            @Override
            public void onResponse(Call<Plans> call, Response<Plans> response) {
                Plans plans = response.body();
                if (checkStatus(plans)) {
                    plansArrayList = new ArrayList<>();
                    plansArrayList = (ArrayList<Plans.Data>) plans.data;
                    setAdapter();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Plans> call, Throwable t) {
                failureError("get plans failed");
            }
        });
    }

    private void setAdapter() {
        if (plansArrayList != null && plansArrayList.size() > 0) {
            if (mAdapter == null) {
                mAdapter = new MyPlanAdapter(this);
            }
            mAdapter.doRefresh(plansArrayList);
            if (rvPlans.getAdapter() == null) {
                rvPlans.setAdapter(mAdapter);
            }
            nestedScrollView.scrollTo(0, 0);
        }
    }
}
