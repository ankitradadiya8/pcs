package com.pcs.model;

public class Product {

    public String name;
    public double unit;
    public double rate;
    public double total;

    public Product(String name, double unit, double rate, double total) {
        this.name = name;
        this.unit = unit;
        this.rate = rate;
        this.total = total;
    }
}
