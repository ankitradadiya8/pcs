package com.pcs.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Outstanding extends GeneralModel {

    @Expose
    @SerializedName("data")
    public List<Data> data;
    @Expose
    @SerializedName("final_total")
    public long finalTotal;

    public static class Data {
        @Expose
        @SerializedName("total")
        public long total;
        @Expose
        @SerializedName("type")
        public String type;
        @Expose
        @SerializedName("username")
        public String username;
    }
}
