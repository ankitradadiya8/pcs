package com.pcs.activity;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.gson.Gson;
import com.pcs.R;
import com.pcs.adapter.TransactionAdapter;
import com.pcs.model.Transaction;
import com.pcs.util.Constants;
import com.pcs.util.EqualSpacingItemDecoration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransactionActivity extends BaseActivity {

    @BindView(R.id.rv_transaction)
    RecyclerView rvTransaction;
    @BindView(R.id.radio_farmer)
    RadioButton radioFarmer;
    @BindView(R.id.radio_vendor)
    RadioButton radioVendor;
    @BindView(R.id.radiogroup)
    RadioGroup radiogroup;

    private TransactionAdapter mAdapter;
    private ArrayList<Transaction.Data> transactionArrayList;
    private ArrayList<Transaction.Data> searchArrayList;
    private RadioButton radioButton;
    private String type = "";
    private boolean isFarmer;
    private boolean isCar = false;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Transaction");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent() != null) {
            isCar = getIntent().getBooleanExtra("isCar", false);
        }

        rvTransaction.setLayoutManager(new LinearLayoutManager(this));
        rvTransaction.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.VERTICAL));

        radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                radioButton = findViewById(checkedId);
                if (radioButton.getText().toString().equals(getString(R.string.purchase))) {
                    isFarmer = true;
                    type = Constants.FARMER;
                } else if (radioButton.getText().toString().equals(getString(R.string.sell))) {
                    isFarmer = false;
                    type = Constants.VENDOR;
                }

                getAllTransaction();
            }
        });
    }

    public void getAllTransaction() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Transaction> call = getService().getAllTransaction(getUserId(), type, 0);
        call.enqueue(new Callback<Transaction>() {
            @Override
            public void onResponse(Call<Transaction> call, Response<Transaction> response) {
                Transaction transaction = response.body();
                if (checkStatus(transaction)) {
                    transactionArrayList = new ArrayList<>();
                    transactionArrayList = (ArrayList<Transaction.Data>) transaction.data;
                    setAdapter(false);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Transaction> call, Throwable t) {
                failureError("get transaction failed");
            }
        });
    }

    private void setAdapter(boolean isSearchData) {
        if (mAdapter == null) {
            mAdapter = new TransactionAdapter(this);
            mAdapter.setCAR(isCar);
        }

        radiogroup.setVisibility(isSearchData ? View.GONE : View.VISIBLE);

        mAdapter.setSearch(isSearchData);
        mAdapter.doRefresh(isSearchData ? searchArrayList : transactionArrayList);

        if (rvTransaction.getAdapter() == null) {
            rvTransaction.setAdapter(mAdapter);
        }
    }

    public void getSearch(String name) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Object> call = getService().getSearchList(name, getUserId());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String json = new Gson().toJson(response.body());
                if (!isEmpty(json) && checkStatus(json)) {
                    Transaction transaction = new Gson().fromJson(json, Transaction.class);
                    searchArrayList = new ArrayList<>();
                    searchArrayList = (ArrayList<Transaction.Data>) transaction.data;
                    setAdapter(true);
                } else {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(json);
                        String msg = null;
                        if (jsonObject.has("data")) {
                            msg = jsonObject.getString("data");
                            toastMessage(msg);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("get search failed");
            }
        });
    }

    public void refreshList() {
        getAllTransaction();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (transactionArrayList != null && transactionArrayList.size() > 0) {
            getAllTransaction();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
//                if (mAdapter != null)
//                    mAdapter.getFilter().filter(query);

                if (!isEmpty(query))
                    getSearch(query);
                else
                    setAdapter(false);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
//                if (mAdapter != null)
//                    mAdapter.getFilter().filter(query);
                if (isEmpty(query))
                    setAdapter(false);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }
}
