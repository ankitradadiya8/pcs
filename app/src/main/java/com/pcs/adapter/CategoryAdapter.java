package com.pcs.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pcs.R;
import com.pcs.activity.BaseActivity;
import com.pcs.activity.CreateMasterActivity;
import com.pcs.activity.MyPlanActivity;
import com.pcs.activity.OnBoardingActivity;
import com.pcs.activity.OutstandingActivity;
import com.pcs.activity.PendingTaskActivity;
import com.pcs.activity.TransactionActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private Context mContext;
    private List<String> mData;

    public CategoryAdapter(Context mContext, List<String> lst) {
        this.mContext = mContext;
        this.mData = lst;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_category, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tvname.setText(mData.get(position));

        holder.tvname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position == 0) {
                    mContext.startActivity(new Intent(mContext, CreateMasterActivity.class));
                } else if (position == 1) {
                    mContext.startActivity(new Intent(mContext, TransactionActivity.class));
                } else if (position == 2) {
                    Intent i = new Intent(mContext, TransactionActivity.class);
                    i.putExtra("isCar", true);
                    mContext.startActivity(i);
                } else if (position == 3) {
                    mContext.startActivity(new Intent(mContext, OnBoardingActivity.class));
                } else if (position == 4) {
                    mContext.startActivity(new Intent(mContext, PendingTaskActivity.class));
                } else if (position == 5) {
                    mContext.startActivity(new Intent(mContext, MyPlanActivity.class));
                } else if (position == 6) {
                    ((BaseActivity) mContext).showNeedHelpDialog();
                } else if (position == 7) {
                    mContext.startActivity(new Intent(mContext, OutstandingActivity.class));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rowname)
        TextView tvname;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}